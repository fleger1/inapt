# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

import logging
from abc import ABC, abstractmethod
from collections import ChainMap
from contextlib import contextmanager
from enum import Enum
from pathlib import Path
from typing import Literal, MutableMapping, ContextManager, TypeVar, Optional, Any, Iterable, Mapping, List

from pydantic import BaseModel

from .argdsl import expand_single_argument
from .installable import FileGenerator, IniFileGenerator, Installable, SymlinkGenerator

logger = logging.getLogger(__name__)

ENV_VOLUMES = "volumes"
ENV_VOLUME_ID = "volume_id"
ENV_VOLUME_DIR = "volume_dir"

DEFAULT_ID = "default"


class Volume(BaseModel, ABC):
    volume: str

    def init(self, app, ep, env: MutableMapping) -> None:
        if ENV_VOLUMES not in env:
            env[ENV_VOLUMES] = {}

    def prep_env(self, env):
        env[ENV_VOLUME_DIR] = "" if env[ENV_VOLUME_ID] == DEFAULT_ID else env[ENV_VOLUME_ID]

    @contextmanager
    def setup(self: V, app, ep, env: MutableMapping) -> ContextManager[V]:
        self.prep_env(env)
        with self._setup(app, ep, env) as s:
            yield s

    @abstractmethod
    @contextmanager
    def _setup(self: V, app, ep, env: MutableMapping) -> ContextManager[V]:
        pass

    @contextmanager
    def _register_volume(self: V, env, path: Path) -> ContextManager[V]:
        env[ENV_VOLUMES][env[ENV_VOLUME_ID]] = path
        logger.debug(f"Volume {env[ENV_VOLUME_ID]} points to {path}")
        try:
            yield self
        finally:
            del env[ENV_VOLUMES][env[ENV_VOLUME_ID]]


V = TypeVar("V", bound=Volume)


class SystemVolume(Volume):
    volume: Literal["system"]
    path: str = "{prefix}/share/{id}/{volume_dir}"

    @contextmanager
    def _setup(self, app, ep, env: MutableMapping):
        with self._register_volume(env, Path(expand_single_argument(self.path, env)).expanduser()) as volume:
            yield volume


class XDGType(str, Enum):
    DATA = "data"
    CONFIG = "config"
    CACHE = "cache"


class XDGVolume(Volume):
    volume: Literal["xdg"]
    type: XDGType = XDGType.DATA
    application: str = "{id}"
    subdirectory: str = "{volume_dir}"

    @contextmanager
    def _setup(self, app, ep, env: MutableMapping):
        from xdg import BaseDirectory
        application = expand_single_argument(self.application, env)
        if self.type is XDGType.CONFIG:
            base_path = Path(BaseDirectory.save_config_path(application))
        elif self.type is XDGType.CACHE:
            base_path = Path(BaseDirectory.save_cache_path(application))
        else:
            base_path = Path(BaseDirectory.save_data_path(application))
        subdirectory = expand_single_argument(self.subdirectory, env)
        volume_path = base_path / subdirectory
        volume_path.mkdir(parents=True, exist_ok=True)
        with self._register_volume(env, Path(expand_single_argument(volume_path, env))) as volume:
            yield volume


class RelayerVolume(Volume, Installable):
    volume: Literal["relayer"]
    application: Optional[str] = None
    lazy: bool = False
    layerset: Optional[str] = None

    @staticmethod
    def with_layerset(element: str, layerset: Optional[str]) -> str:
        if not layerset:
            return element
        return f"{element}.{layerset}"

    @contextmanager
    def _setup(self, app, ep, env: MutableMapping):
        from relayer import RelayerController
        rc = RelayerController(self.compute_application(env))
        if hasattr(rc.modifiers, 'layerset'):
            rc.modifiers.layerset = self.layerset
        elif self.layerset:
            logger.warning("Relayer version too old to support layer sets!")
        with rc.auto_mount(lazy=self.lazy) as path:
            with self._register_volume(env, path) as volume:
                yield volume

    def compute_application(self, env):
        if self.application is None:
            application = "{id}"
            if env[ENV_VOLUME_DIR] != "":
                application += "_{volume_dir}"
        else:
            application = self.application
        return expand_single_argument(application, env)

    def do_install(self, app, env: MutableMapping[str, Any],
                   props: Mapping[str, List[str]]) -> Iterable[FileGenerator]:
        self.prep_env(env)

        settings = {}
        if env[ENV_VOLUME_ID] == DEFAULT_ID:
            settings["Name"] = expand_single_argument("{name}", env)
        else:
            settings["Name"] = expand_single_argument("{name} ({volume_id})", env)

        if app.desktop_entry:
            settings["DesktopEntry"] = str(
                app.prefix / "share" / "applications" / f"{expand_single_argument(app.desktop_entry, env)}.desktop")

        if len(app.entry_points) == 1:
            ep_id, ep = next(iter(app.entry_points.items()))
            if ep.private:
                ep = None
        else:
            ep_id = "main"
            ep = app.entry_points.get(ep_id, None)

        if ep is not None:
            new_env = ChainMap({}, env)
            ep.init(app, new_env, ep_id)
            settings["Command"] = str(ep.compute_path(app, new_env))

        relayer_app = self.compute_application(env)

        yield IniFileGenerator({"RelayerSettings": settings},
                               app.prefix / "lib" / "relayer" / f"{relayer_app}.conf")

        from distutils.util import strtobool

        if strtobool(props.get("generate_layer", ("0",))[-1]):
            from configparser import RawConfigParser
            layer = RawConfigParser(allow_no_value=True)
            layer.optionxform = lambda x: x

            directory = props.get("directory", ("data",))[-1]

            provides = set(props.get("provides", ()))
            provides.add(relayer_app)
            layer.update({
                "Layers": {
                    "Name": settings['Name'],
                    "Description": f"{settings['Name']} base data layer",
                    "Version": props.get("version", ("1.0",))[-1],
                    "Layer1": "base",
                },
                "base:Layer": {
                    "Type": "static",
                    "SourceDirectory": app.prefix / "share" / app.id / env[ENV_VOLUME_DIR] / directory
                },
                "base:Provides": {k: None for k in provides}
            })

            depends = set(props.get("depends", ()))
            if len(depends) > 0:
                layer.update({"Depends": {k: None for k in depends}})

            conflicts = set(props.get("conflicts", ()))
            if len(conflicts) > 0:
                layer.update({"Conflicts": {k: None for k in conflicts}})

            relayer_path = app.prefix / "lib" / "relayer" / relayer_app
            layer_base_name = f"{props.get('priority', ('00',))[-1]}-{relayer_app}.layer"

            yield IniFileGenerator(layer, relayer_path / layer_base_name)

            if strtobool(props.get("enable_layer", ("1",))[-1]):
                for layerset in set(props.get("layerset", (self.layerset,))):
                    yield SymlinkGenerator(relayer_path / self.with_layerset('enabled', layerset) / layer_base_name,
                                           Path("..") / layer_base_name)
