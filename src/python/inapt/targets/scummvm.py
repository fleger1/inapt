# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

from abc import ABC, abstractmethod
from contextlib import contextmanager
from pathlib import Path
from typing import Literal, Union, List, Any, MutableMapping, ContextManager, Iterable, Optional

from pydantic import BaseModel

from ..actions import Action, SubProcessAction, CommonAction
from ..application import Application, EntryPoint
from ..argdsl import expand_single_argument
from ..features import WMClassFeature, SDLFeature, Features
from ..game import GameRepositoryMixin, GameActionMixin
from ..model import ExpandedPath


class VMGame(BaseModel):
    target: str
    path: Union[str, Path]
    game: Optional[str] = None


class VMAction(GameRepositoryMixin[VMGame],
               GameActionMixin[VMGame]):
    pass


class VMRunAction(SubProcessAction, VMAction):
    action: Literal["run"]

    @contextmanager
    def yield_command(self, app: VMApplication, ep, env) -> ContextManager[List[Any]]:
        yield [app.executable(), "{cmd_args}", self.resolve_game(app, ep).target]


class VMAddAction(SubProcessAction, VMAction):
    action: Literal["add"]

    @contextmanager
    def yield_command(self, app: VMApplication, ep, env) -> ContextManager[List[Any]]:
        game = self.resolve_game(app, ep)
        cmd = [app.executable(), f"--path={str(Path(expand_single_argument(game.path, env)).expanduser())}", "--add"]
        if game.game is not None:
            cmd.append(f"--game={game.game}")
        yield cmd


VMActionType = Union[CommonAction, VMAddAction, VMRunAction, str]


class VMEntryPoint(GameRepositoryMixin[VMGame],
                   EntryPoint[VMActionType]):
    pass


class VMApplication(GameRepositoryMixin[VMGame],
                    Application[VMActionType, VMEntryPoint],
                    ABC):
    features: Features = Features(wm_class=WMClassFeature(), sdl=SDLFeature())

    @abstractmethod
    def executable(self) -> Path:
        pass

    def transform_actions(self, actions, ep, env) -> Iterable[Action]:
        for action in actions:
            if isinstance(action, str):
                yield VMAddAction(game=action, action="add")
                yield VMRunAction(game=action, action="run")
            else:
                yield action


class ScummVMApplication(VMApplication):
    target: Literal['scummvm']
    scummvm: ExpandedPath = Path("scummvm")

    def prepare_environment(self) -> MutableMapping[str, Any]:
        env = super().prepare_environment()
        env["scummvm"] = self.scummvm
        return env

    def executable(self) -> Path:
        return self.scummvm
