# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

from contextlib import contextmanager
from pathlib import Path
from typing import Literal, Union, List, Any, MutableMapping, ContextManager, Iterable

from ..actions import Action, SubProcessAction, CommonAction
from ..application import Application, EntryPoint
from ..argdsl import expand_single_argument
from ..model import ExpandedPath


class AGSAction(SubProcessAction):
    action: Literal["ags"]
    gamedir: Union[str, Path]

    @contextmanager
    def yield_command(self, app: AGSApplication, ep, env) -> ContextManager[List[Any]]:
        yield ["{ags}", "{cmd_args}", "--", str(Path(expand_single_argument(self.gamedir, env)).expanduser())]


AGSActionType = Union[CommonAction, AGSAction, str]


class AGSApplication(Application[AGSActionType, EntryPoint[AGSActionType]]):
    target: Literal['ags']
    ags: ExpandedPath = Path("ags")

    def prepare_environment(self) -> MutableMapping[str, Any]:
        env = super().prepare_environment()
        env["ags"] = self.ags
        return env

    def transform_actions(self, actions, ep, env) -> Iterable[Action]:
        for action in actions:
            if isinstance(action, str):
                action = AGSAction(gamedir=action, action="ags")
            yield action
