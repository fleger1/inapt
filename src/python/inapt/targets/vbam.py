# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

from contextlib import contextmanager
from pathlib import Path
from typing import Literal, Union, List, Any, MutableMapping, ContextManager, Iterable

from ..actions import Action, SubProcessAction, CommonAction
from ..application import Application, EntryPoint
from ..argdsl import expand_single_argument
from ..features import WMClassFeature, SDLFeature, GTKFeature, Features
from ..model import ExpandedPath


class VBAMAction(SubProcessAction):
    action: Literal["vbam"]
    rom: Union[str, Path]

    @contextmanager
    def yield_command(self, app: VBAMApplication, ep, env) -> ContextManager[List[Any]]:
        cmd = ["{vbam}"]
        # GTKFeature may have been disabled by the user
        if "gtk_args" in env:
            cmd.append("{gtk_args}")
        cmd.append(str(Path(expand_single_argument(self.rom, env)).expanduser()))
        cmd.append("{cmd_args}")
        yield cmd


VBAMActionType = Union[CommonAction, VBAMAction, str]


class VBAMApplication(Application[VBAMActionType, EntryPoint[VBAMActionType]]):
    target: Literal['vbam']

    features: Features = Features(wm_class=WMClassFeature(), sdl=SDLFeature(), gtk=GTKFeature())
    vbam: ExpandedPath = Path("visualboyadvance-m")

    def prepare_environment(self) -> MutableMapping[str, Any]:
        env = super().prepare_environment()
        env["vbam"] = self.vbam
        return env

    def transform_actions(self, actions, ep, env) -> Iterable[Action]:
        for action in actions:
            if isinstance(action, str):
                action = VBAMAction(rom=action, action="vbam")
            yield action
