# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

from contextlib import contextmanager
from pathlib import Path
from typing import Literal, Union, List, Any, MutableMapping, ContextManager, Iterable

from ..actions import Action, SubProcessAction, CommonAction
from ..application import Application, EntryPoint
from ..argdsl import expand_single_argument
from ..features import WMClassFeature, SDLFeature, GTKFeature, Features
from ..model import ExpandedPath


class TapeAction(SubProcessAction):
    action: Literal["tape"]
    tape: Union[str, Path]

    @contextmanager
    def yield_command(self, app: FuseApplication, ep, env) -> ContextManager[List[Any]]:
        cmd = ["{fuse}"]
        # GTKFeature may have been disabled by the user
        if "gtk_args" in env:
            cmd.append("{gtk_args}")
        cmd.append("--tape")
        cmd.append(str(Path(expand_single_argument(self.tape, env)).expanduser()))
        cmd.append("{cmd_args}")
        yield cmd


FuseActionType = Union[CommonAction, TapeAction, str]


class FuseApplication(Application[FuseActionType, EntryPoint[FuseActionType]]):
    target: Literal['fuse']

    features: Features = Features(wm_class=WMClassFeature(), sdl=SDLFeature(), gtk=GTKFeature())
    fuse: ExpandedPath = Path("fuse")

    def prepare_environment(self) -> MutableMapping[str, Any]:
        env = super().prepare_environment()
        env["fuse"] = self.fuse
        return env

    def transform_actions(self, actions, ep, env) -> Iterable[Action]:
        for action in actions:
            if isinstance(action, str):
                action = TapeAction(tape=action, action="tape")
            yield action
