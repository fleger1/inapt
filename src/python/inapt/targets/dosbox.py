# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

import itertools
import logging
from abc import ABC
from collections import ChainMap
from contextlib import contextmanager
from enum import Enum
from pathlib import PureWindowsPath, Path
from subprocess import list2cmdline
from typing import Literal, Optional, List, Union, Iterable, MutableMapping, ContextManager, Any, Annotated

from pydantic import Field, AfterValidator

from ..actions import Action, SubProcessAction, CommonAction
from ..application import Application, EntryPoint
from ..argdsl import expand_single_argument
from ..features import WMClassFeature, SDLFeature, Features
from ..model import ExpandedPath
from ..mountpoints import MountPoint, MountPointRepositoryMixin
from ..scripting import ScriptTemplate, ScriptActionMixin, CommonScriptTemplate, \
    is_common_script_template, ScriptRepositoryMixin
from ..utils import union_from_entry_points, with_discriminator

logger = logging.getLogger(__name__)


class DOSBoxMountType(str, Enum):
    DIR = "dir"
    CDROM = "cdrom"
    FLOPPY = "floppy"
    ISO = "iso"


class DOSBoxMountPoint(MountPoint):
    mount_type: DOSBoxMountType = DOSBoxMountType.DIR


def dos_drive_validator(v):
    assert v.drive, 'invalid dos drive'
    assert v != DOSPath("Z:"), 'dos drive letter reserved'
    return v

DOSPath = PureWindowsPath
DOSDrive = Annotated[DOSPath, AfterValidator(dos_drive_validator)]


class BaseDOSBoxTemplate(ScriptTemplate, ABC):
    pass


class IfNotExistTemplate(BaseDOSBoxTemplate):
    template: Literal["if_not_exist"]
    file: DOSPath
    script: List[DOSBoxTemplate] = []
    else_script: List[DOSBoxTemplate] = Field(default_factory=list, alias="else")

    def lines(self, app, ep, env, action: DOSBoxBatchAction) -> Iterable[str]:
        exist_label = action.auto_goto_label(env)
        end_label = action.auto_goto_label(env)
        yield f"IF EXIST {self.file} GOTO {exist_label}"
        new_env = ChainMap({}, env)
        new_env["else_label"] = exist_label
        new_env["end_label"] = end_label
        yield from action.process_script(app, ep, new_env, self.script)
        yield f"GOTO {end_label}"
        yield f":{exist_label}"
        yield from action.process_script(app, ep, new_env, self.else_script)
        yield f":{end_label}"
        yield ""


class IfExistTemplate(BaseDOSBoxTemplate):
    template: Literal["if_exist"]
    file: DOSPath
    script: List[DOSBoxTemplate] = []
    else_script: List[DOSBoxTemplate] = Field(default_factory=list, alias="else")

    def lines(self, app, ep, env, action: DOSBoxBatchAction) -> Iterable[str]:
        exist_label = action.auto_goto_label(env)
        end_label = action.auto_goto_label(env)
        yield f"IF EXIST {self.file} GOTO {exist_label}"
        new_env = ChainMap({}, env)
        new_env["exist_label"] = exist_label
        new_env["end_label"] = end_label
        yield from action.process_script(app, ep, new_env, self.else_script)
        yield f"GOTO {end_label}"
        yield f":{exist_label}"
        yield from action.process_script(app, ep, new_env, self.script)
        yield f":{end_label}"
        yield ""


class DieIfNotExistTemplate(BaseDOSBoxTemplate):
    template: Literal["die_if_not_exist"]
    file: DOSPath
    message: Optional[str] = None
    pause: bool = False
    exit_code: int = 1

    def lines(self, app, ep, env, action: DOSBoxBatchAction) -> Iterable[str]:
        end_label = action.auto_goto_label(env)
        yield f"IF EXIST {self.file} GOTO {end_label}"
        if self.message is not None:
            for line in self.message.splitlines():
                yield f"ECHO {line}"
        if self.pause:
            yield "PAUSE"
        yield f"EXIT {self.exit_code}"
        yield f":{end_label}"
        yield ""


class EnvCopyTemplate(BaseDOSBoxTemplate):
    template: Literal["env_copy"]
    variables: List[str] = []

    def lines(self, app, ep, env, action: ScriptActionMixin) -> Iterable[str]:
        if "env" in env:
            exec_env = env["env"]
            for var in self.variables:
                var = expand_single_argument(var, env)
                if var in exec_env:
                    yield f"SET {var}={exec_env[var]}"


DOSBoxSpecificTemplate = with_discriminator(union_from_entry_points("inapt.targets.dosbox.templates"), 'template')
DOSBoxTemplate = Union[DOSBoxSpecificTemplate, CommonScriptTemplate, List[str], str]
IfNotExistTemplate.model_rebuild()
IfExistTemplate.model_rebuild()


def is_template(candidate):
    return isinstance(candidate, str) \
           or isinstance(candidate, List) \
           or isinstance(candidate, BaseDOSBoxTemplate) \
           or is_common_script_template(candidate)


class DOSBoxBatchAction(ScriptActionMixin,
                        ScriptRepositoryMixin[DOSBoxTemplate],
                        SubProcessAction):
    action: Literal["batch"]
    clear: bool = True
    echo: bool = False
    exit: bool = True

    @staticmethod
    def auto_goto_label(env):
        try:
            goto_counter = env["goto_counter"]
        except KeyError:
            goto_counter = itertools.count()
            env["goto_counter"] = goto_counter
        return f"AG{next(goto_counter):06}"

    def main_script(self, app: DOSBoxApplication, ep, env) -> Iterable[str]:
        if not self.echo:
            yield "@echo off"
            yield ""

        # Setup mountpoints
        for drive, mountpoint in app.mount_points.items():
            volume = env["volumes"][mountpoint.volume]
            yield f"mount -u {drive.drive}"
            if mountpoint.mount_type is DOSBoxMountType.ISO:
                yield f"imgmount {drive.drive} {list2cmdline((str(volume),))}"
            else:
                yield f"mount -t {mountpoint.mount_type.value} {drive.drive} {list2cmdline((str(volume),))}"
            yield ""

        if self.clear:
            yield "cls"

        if app.init_script:
            yield from self.process_script(app, ep, env, app.init_script)

        yield from self.process_script(app, ep, env, self.script)

        if self.exit:
            yield ""
            yield "exit"

    @contextmanager
    def yield_command(self, app: DOSBoxApplication, ep, env) -> ContextManager[Optional[List[Any]]]:
        # Build a temporary dosbox.conf file
        from configparser import ConfigParser
        dosbox_conf = ConfigParser(allow_no_value=True, interpolation=None)
        dosbox_conf.optionxform = lambda s: s

        if ep.name is not None:
            dosbox_conf["SDL"] = {"titlebar": ep.name}

        if app.name is not None:
            dosbox_conf["DOSBOX"] = {"title": app.name}

        # Must be the last section
        dosbox_conf.add_section("AUTOEXEC")
        dosbox_conf.set("AUTOEXEC", "\n".join(self.main_script(app, ep, env)), None)

        from tempfile import NamedTemporaryFile
        from xdg import BaseDirectory
        with NamedTemporaryFile(mode='w', prefix=app.id, suffix=".conf",
                                dir=BaseDirectory.get_runtime_dir(), delete=False) as f:
            dosbox_conf.write(f)
        logger.info(f"Temp dosbox.conf file generated as {f.name}")
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"Temp dosbox.conf content:\n{Path(f.name).read_text()}")

        try:
            yield [
                "{dosbox}", "-userconf",
                "-conf", "{prefix}/lib/{id}/dosbox.conf",
                "-conf", "{sysconfdir}/{id}/dosbox.conf",
                "-conf", "{prefix}/lib/{id}/dosbox-{ep[id]}.conf",
                "-conf", "{sysconfdir}/{id}/dosbox-{ep[id]}.conf",
                "-conf", f"{BaseDirectory.xdg_config_home}/""{id}/dosbox.conf",
                "-conf", f"{BaseDirectory.xdg_config_home}/""{id}/dosbox-{ep[id]}.conf",
                "-conf", f.name
            ]
        finally:
            Path(f.name).unlink(missing_ok=True)


DOSBoxActionType = Union[CommonAction, DOSBoxBatchAction, DOSBoxTemplate]


class DOSBoxEntryPoint(ScriptRepositoryMixin[DOSBoxTemplate], EntryPoint[DOSBoxActionType]):
    pass


class DOSBoxApplication(MountPointRepositoryMixin[DOSDrive, DOSBoxMountPoint],
                        ScriptRepositoryMixin[DOSBoxTemplate],
                        Application[DOSBoxActionType, DOSBoxEntryPoint]):
    target: Literal["dosbox"]
    features: Features = Features(wm_class=WMClassFeature(), sdl=SDLFeature())
    dosbox: ExpandedPath = Path("dosbox")
    init_script: List[DOSBoxTemplate] = []

    def init(self):
        # Builtin entry points
        self.entry_points.setdefault("cmd", DOSBoxEntryPoint(name="DOS Command Prompt", private=True,
                                                             actions=[DOSBoxBatchAction(action="batch",
                                                                                        echo=True,
                                                                                        clear=False,
                                                                                        exit=False,
                                                                                        script=[["{cmd_args}"]])]))

    def prepare_environment(self) -> MutableMapping[str, Any]:
        env = super().prepare_environment()
        env["dosbox"] = self.dosbox
        return env

    def transform_actions(self, actions, ep, env: MutableMapping[str, Any]) -> Iterable[Action]:
        current_cmd = None
        # Merge consecutive script contents into single DOSBoxBatchActions
        for action in actions:
            if is_template(action):
                if current_cmd is None:
                    current_cmd = DOSBoxBatchAction(script=[], action="batch")
                current_cmd.script.append(action)
            else:
                if current_cmd is not None:
                    yield current_cmd
                    current_cmd = None
                yield action
        if current_cmd is not None:
            yield current_cmd
