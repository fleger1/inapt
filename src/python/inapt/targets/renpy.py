# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

from contextlib import contextmanager
from pathlib import Path
from typing import Literal, Union, List, Any, MutableMapping, ContextManager, Iterable

from pydantic import BaseModel

from ..actions import Action, SubProcessAction, CommonAction
from ..application import Application, EntryPoint
from ..argdsl import expand_single_argument
from ..features import WMClassFeature, SDLFeature, Features
from ..game import GameRepositoryMixin, GameActionMixin
from ..model import ExpandedPath


class RenPyGame(BaseModel):
    basedir: Union[str, Path]
    savedir: Union[str, Path]


class RenPyAction(GameRepositoryMixin[RenPyGame],
                  GameActionMixin[RenPyGame]):
    pass


class RunAction(SubProcessAction, RenPyAction):
    action: Literal["run"]

    @contextmanager
    def yield_command(self, app: RenPyApplication, ep, env) -> ContextManager[List[Any]]:
        game = self.resolve_game(app, ep)
        yield [
            "{renpy}",
            "--savedir",
            str(Path(expand_single_argument(game.savedir, env)).expanduser()),
            str(Path(expand_single_argument(game.basedir, env)).expanduser()),
            "{cmd_args}"
        ]


RenPyActionType = Union[CommonAction, RunAction, str]


class RenPyEntryPoint(GameRepositoryMixin[RenPyGame],
                      EntryPoint[RenPyActionType]):
    pass


class RenPyApplication(GameRepositoryMixin[RenPyGame],
                       Application[RenPyActionType, RenPyEntryPoint]):
    target: Literal['renpy']

    features: Features = Features(wm_class=WMClassFeature(), sdl=SDLFeature())
    renpy: ExpandedPath = Path("renpy")

    def prepare_environment(self) -> MutableMapping[str, Any]:
        env = super().prepare_environment()
        env["renpy"] = self.renpy
        return env

    def transform_actions(self, actions, ep, env) -> Iterable[Action]:
        for action in actions:
            if isinstance(action, str):
                action = RunAction(game=action, action="run")
            yield action
