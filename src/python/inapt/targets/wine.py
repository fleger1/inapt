# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

import itertools
import logging
import shutil
from abc import ABC
from collections import ChainMap
from contextlib import contextmanager
from enum import Enum
from pathlib import PureWindowsPath, Path, PurePosixPath
from subprocess import list2cmdline
from typing import Literal, Optional, List, Union, MutableMapping, Any, Dict, ContextManager, Iterable, Pattern, \
    Iterator, Sequence, Annotated

from pydantic import BaseModel, Field, AfterValidator

from ..actions import Action, SubProcessAction, CommonAction
from ..application import Application, EntryPoint
from ..argdsl import expand_single_argument
from ..model import ExpandedPath
from ..mountpoints import MountPoint, MountPointRepositoryMixin
from ..scripting import ScriptTemplate, ScriptActionMixin, is_common_script_template, CommonScriptTemplate, \
    ScriptRepositoryMixin
from ..utils import union_from_entry_points, with_discriminator
from ..volumes import XDGVolume, XDGType

logger = logging.getLogger(__name__)


class WineMountPoint(MountPoint):
    pass

def validate_wine_drive(v) -> WineDrive:
    assert v.drive, 'invalid dos drive'
    assert v != WinePath("Z:") and v != WinePath("C:"), 'dos drive letter reserved'
    return v


WinePath = PureWindowsPath
WineDrive = Annotated[WinePath, AfterValidator(validate_wine_drive)]


class BaseWineTemplate(ScriptTemplate, ABC):
    pass


class RegAdd(BaseModel):
    data: str
    type: str = "REG_SZ"
    separator: Optional[str] = None


class RegistryTemplate(BaseWineTemplate, BaseModel):
    template: Literal['registry']
    add: Dict[str, Dict[str, Union[RegAdd, str]]] = {}
    delete: Dict[str, Optional[List[Optional[str]]]] = {}

    @staticmethod
    def __transform_regadd(regadd: Union[RegAdd, str]) -> RegAdd:
        if isinstance(regadd, str):
            regadd = RegAdd(data=regadd)
        return regadd

    def lines(self, app, ep, env, action) -> Iterable[str]:
        # Add
        for regkey, valuedef in self.add.items():
            cmd_start = ("reg.exe", "add", regkey, '/F')
            if valuedef:
                for valuename, regadd in valuedef.items():
                    regadd = self.__transform_regadd(regadd)
                    cmd = list(cmd_start)
                    if valuename:
                        cmd.extend(("/V", valuename))
                    else:
                        cmd.append("/VE")
                    cmd.extend(("/T", regadd.type))
                    if regadd.separator:
                        cmd.extend(("/S", regadd.separator))
                    cmd.extend(("/D", expand_single_argument(regadd.data, env)))
                    yield f"{list2cmdline(cmd)}"
            else:
                # Create the key with no values
                yield f"{list2cmdline(cmd_start)}"

        # Delete
        for regkey, valuedef in self.delete.items():
            cmd_start = ("reg.exe", "delete", regkey, '/F')
            if valuedef is None:
                # If valuedef is explicitly None, delete the whole key
                yield f"{list2cmdline(cmd_start + ('/VA',))}"
            else:
                for valuename in valuedef:
                    cmd = list(cmd_start)
                    if valuename:
                        cmd.extend(("/V", valuename))
                    else:
                        cmd.append("/VE")
                    yield f"{list2cmdline(cmd)}"


class IfNotExistTemplate(BaseWineTemplate):
    template: Literal["if_not_exist"]
    file: WinePath
    script: List[WineTemplate] = []
    else_script: List[WineTemplate] = Field(default_factory=list, alias="else")

    def lines(self, app, ep, env, action: WineCMDAction) -> Iterable[str]:
        yield list2cmdline(("IF", "NOT", "EXIST", str(self.file), "("))
        new_env = ChainMap({}, env)
        yield from action.process_script(app, ep, new_env, self.script)
        if self.else_script:
            yield ") ELSE ("
            yield from action.process_script(app, ep, new_env, self.else_script)
            yield ")"
        else:
            yield ")"
        yield ""


class IfExistTemplate(BaseWineTemplate):
    template: Literal["if_exist"]
    file: WinePath
    script: List[WineTemplate] = []
    else_script: List[WineTemplate] = Field(default_factory=list, alias="else")

    def lines(self, app, ep, env, action: WineCMDAction) -> Iterable[str]:
        yield list2cmdline(("IF", "EXIST", str(self.file), "("))
        new_env = ChainMap({}, env)
        yield from action.process_script(app, ep, new_env, self.script)
        if self.else_script:
            yield ") ELSE ("
            yield from action.process_script(app, ep, new_env, self.else_script)
            yield ")"
        else:
            yield ")"
        yield ""


class DieIfNotExistTemplate(BaseWineTemplate):
    template: Literal["die_if_not_exist"]
    file: WinePath
    message: Optional[str] = None
    pause: bool = False
    exit_code: int = 1

    def lines(self, app, ep, env, action: WineCMDAction) -> Iterable[str]:
        yield list2cmdline(("IF", "NOT", "EXIST", str(self.file), "("))
        if self.message is not None:
            for line in self.message.splitlines():
                yield f"ECHO {line}"
                yield f"ECHO {line}"
        if self.pause:
            yield "PAUSE"
        yield f"EXIT {self.exit_code}"
        yield ")"
        yield ""


WineSpecificTemplate = with_discriminator(union_from_entry_points("inapt.targets.wine.templates"), 'template')
WineTemplate = Union[WineSpecificTemplate, CommonScriptTemplate, List[str], str]
IfNotExistTemplate.model_rebuild()
IfExistTemplate.model_rebuild()


def is_template(candidate):
    return isinstance(candidate, str) \
           or isinstance(candidate, List) \
           or isinstance(candidate, BaseWineTemplate) \
           or is_common_script_template(candidate)


def winepathw(unix_path: Union[str, PurePosixPath]) -> PureWindowsPath:
    if isinstance(unix_path, str):
        unix_path = PurePosixPath(unix_path)
    wine_path = PureWindowsPath(str(unix_path))
    if unix_path.is_absolute():
        wine_path = 'Z:' / wine_path
    return wine_path


class ParamType(str, Enum):
    TEXT = "text"
    PATH = "path"


class CmdArgsTransform(BaseModel):
    positionals: List[ParamType] = []
    options: Optional[Pattern] = None
    text_params: Optional[Pattern] = None
    path_params: Optional[Pattern] = None
    path_substrings: Optional[Pattern] = None
    end_options_marker: Optional[str] = None

    def __positional_types(self) -> Iterator[ParamType]:
        if not self.positionals:
            yield from itertools.repeat(ParamType.TEXT)
        else:
            yield from self.positionals
            yield from itertools.repeat(self.positionals[-1])

    def transform_arguments(self, arguments: List[str]) -> List[str]:
        args = []

        parsing_options = True
        prev_option = None

        positional_types = self.__positional_types()

        for arg in arguments:
            if parsing_options and self.text_params and prev_option and self.text_params.match(prev_option):
                # Do not transform, do not treat as an option
                args.append(arg)
                prev_option = None
            elif parsing_options and self.path_params and prev_option and self.path_params.match(prev_option):
                # Transform but do not treat as an option
                args.append(str(winepathw(arg)))
                prev_option = None
            elif parsing_options and self.end_options_marker and arg == self.end_options_marker:
                # Treat remaining arguments as positional
                args.append(arg)
                prev_option = None
                parsing_options = False
            elif parsing_options and self.path_substrings and (m := self.path_substrings.match(arg)):
                # Substring is a path
                # noinspection PyUnboundLocalVariable
                args.append(f"{m.group(1)}{winepathw(m.group(2))}{m.group(3) if len(m.groups()) > 2 else ''}")
                prev_option = None
            elif parsing_options and self.options and self.options.match(arg):
                # Treat as an option
                args.append(arg)
                prev_option = arg
            else:
                # Positional argument
                arg_type = next(positional_types)
                args.append(str(winepathw(arg)) if arg_type is ParamType.PATH else arg)
                prev_option = None
        return args


class BaseWineAction(Action, ABC):
    cmd_args_transform: Optional[CmdArgsTransform] = None

    def run(self, app, ep, env: MutableMapping):
        if "cmd_args" in env and self.cmd_args_transform:
            env["cmd_args"] = self.cmd_args_transform.transform_arguments(env["cmd_args"])
        return super().run(app, ep, env)


class WineCMDAction(ScriptActionMixin[WineTemplate],
                    ScriptRepositoryMixin[WineTemplate],
                    BaseWineAction,
                    SubProcessAction):
    action: Literal['cmd']

    def join(self, items: Sequence[str]) -> str:
        return list2cmdline(items)

    @contextmanager
    def yield_command(self, app, ep, env) -> ContextManager[Optional[List[str]]]:
        from tempfile import NamedTemporaryFile
        from xdg import BaseDirectory
        with NamedTemporaryFile(mode="w", dir=BaseDirectory.get_runtime_dir(strict=False),
                                prefix=app.id, suffix=".cmd", newline="\r\n", delete=False) as cmd:
            cmd.writelines(f"{line}\n" for line in self.process_script(app,
                                                                       ep,
                                                                       env,
                                                                       itertools.chain(app.build_init_script(env),
                                                                                       self.script)))
        logger.info(f"Temp batch file generated as {cmd.name}")
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"Temp batch file content:\n{Path(cmd.name).read_text()}")
        try:
            yield ["{wine}", "cmd", "/c", cmd.name]
        finally:
            Path(cmd.name).unlink(missing_ok=True)


class WineAction(BaseWineAction, SubProcessAction):
    action: Literal['wine']
    command: Union[List[str], str]

    @contextmanager
    def yield_command(self, app, ep, env) -> ContextManager[List[str]]:
        import shlex
        command = shlex.split(self.command) if isinstance(self.command, str) else self.command
        yield ["{wine}", "cmd", "/c"] + command


class WineServerAction(BaseWineAction, SubProcessAction):
    action: Literal['wineserver']
    arguments: Union[List[str], str]

    @contextmanager
    def yield_command(self, app, ep, env) -> ContextManager[List[str]]:
        import shlex
        arguments = shlex.split(self.arguments) if isinstance(self.arguments, str) else self.arguments
        yield ["{wineserver}"] + arguments


class WineTricksAction(SubProcessAction):
    action: Literal['winetricks']
    verbs: List[str]
    fast_verify: bool = True
    gui: bool = True
    winetricks: Path = Path("winetricks")

    @contextmanager
    def yield_command(self, app: WineApplication, ep, env: MutableMapping) -> ContextManager[Optional[List[Any]]]:
        verbs = set(self.verbs)
        force = len(verbs) == 0

        # Because winetricks is very slow, implement a faster way to check
        # if verbs have already been run.
        if len(verbs) and self.fast_verify:
            winetricks_log = Path(env["env"]["WINEPREFIX"]) / "winetricks.log"
            if winetricks_log.is_file():
                with winetricks_log.open("r") as f:
                    for line in f:
                        try:
                            verbs.remove(line.rstrip("\n"))
                            if len(verbs) == 0:
                                break
                        except KeyError:
                            pass

        if len(verbs) or force:
            env["env"]["WINE"] = str(app.wine)
            env["env"]["WINESERVER"] = str(app.wineserver)
            cmd = [self.winetricks]
            if not force:
                cmd.append("-q")
                if self.gui:
                    cmd.append("--gui")
                cmd.extend(verbs)
            else:
                cmd.append("{cmd_args}")
            yield cmd
        else:
            yield None


class WineArch(str, Enum):
    WIN32 = "win32"
    WIN64 = "win64"


WineActionModelType = Union[CommonAction, WineAction, WineServerAction,
                            WineCMDAction, WineTricksAction, WineTemplate]


class WineEntryPoint(ScriptRepositoryMixin[WineTemplate],
                     EntryPoint[WineActionModelType]):
    pass


class WineDXVK(BaseModel):
    enabled: bool = True
    force_uninstall: bool = False
    dxvk_path: ExpandedPath = Path("/usr/share/dxvk")
    dxgi: bool = True
    symlink: bool = True
    state_cache: bool = True
    state_cache_path: Optional[str] = "{volumes[dxvk_cache]}"
    log_path: Optional[str] = "none"
    log_level: Optional[str] = None
    config_file: Optional[str] = None


class WineApplication(MountPointRepositoryMixin[WineDrive, WineMountPoint],
                      ScriptRepositoryMixin[WineTemplate],
                      Application[WineActionModelType, WineEntryPoint]):
    target: Literal["wine"]
    wine_arch: Optional[WineArch] = None
    wine: ExpandedPath = Path("wine")
    wineserver: ExpandedPath = Path("wineserver")
    init_script: List[WineTemplate] = []
    wine_nine: Optional[bool] = None
    dxvk: Optional[WineDXVK] = None

    def init(self):
        # Create a special wineprefix volume if none is specified
        # Adhere to https://wiki.winehq.org/Bottling_Standards prefix location rules
        self.volumes.setdefault("wineprefix", XDGVolume(volume="xdg",
                                                        application="wineprefixes",
                                                        subdirectory="{id}"))

        # Create cache volume if dxvk is enabled
        if self.dxvk is not None and self.dxvk.enabled and not self.dxvk.force_uninstall:
            self.volumes.setdefault("dxvk_cache", XDGVolume(volume="xdg", type=XDGType.CACHE))

        # Builtin entry points
        self.entry_points.setdefault("cmd", WineEntryPoint(name="Wine Command Prompt", private=True, 
                                                           actions=[["cmd", "{cmd_args}"]]))
        self.entry_points.setdefault("winecfg", WineEntryPoint(name="Wine configuration", private=True,
                                                               actions=[["winecfg", "{cmd_args}"]]))
        self.entry_points.setdefault("regedit", WineEntryPoint(name="Registry Editor", private=True,
                                                               actions=[["regedit", "{cmd_args}"]]))
        self.entry_points.setdefault("winetricks", WineEntryPoint(name="Wine Tricks", private=True,
                                                                  actions=[WineTricksAction(action="winetricks",
                                                                                            verbs=[])]))

    def build_init_script(self, env) -> Iterable[WineTemplate]:
        # Programmatic init
        yield from env.get('wine_preinit_script', ())

        # Custom init
        yield from self.init_script

    def prepare_environment(self) -> MutableMapping[str, Any]:
        env = super().prepare_environment()
        env["wine"] = self.wine
        env["wineserver"] = self.wineserver
        exec_env: Dict[str, str] = env["env"]

        # Disable file associations and menu item creations
        dll_overrides = exec_env.get("WINEDLLOVERRIDES", "")
        if dll_overrides:
            dll_overrides += ","
        dll_overrides += "winemenubuilder.exe=d"
        exec_env["WINEDLLOVERRIDES"] = dll_overrides

        if self.wine_arch is not None:
            exec_env["WINEARCH"] = self.wine_arch.value

        # Pre-init script
        # noinspection PyListCreation
        wine_preinit_script = []

        # Disable Wine Menu Builder
        wine_preinit_script.append(RegistryTemplate(template='registry', add={
            r"HKEY_CURRENT_USER\Software\Wine\DllOverrides": {
                "winemenubuilder.exe": ""
            }
        }))

        # Try loading user's wine.inf
        from xdg import BaseDirectory
        inf = BaseDirectory.load_first_config("wine.inf")
        if inf is not None:
            wine_preinit_script.append(
                ["rundll32.exe", "setupapi.dll,InstallHinfSection", "DefaultInstall", "128", inf]
            )

        # Wine Nine
        if self.wine_nine is True:
            wine_preinit_script.append('ninewinecfg -e')
        elif self.wine_nine is False:
            wine_preinit_script.append('ninewinecfg -d')

        env["wine_preinit_script"] = wine_preinit_script

        return env

    def transform_actions(self, actions, ep, env: MutableMapping[str, Any]) -> Iterable[Action]:
        current_cmd = None
        # Merge consecutive script contents into single WineCMDActions
        for action in actions:
            if is_template(action):
                if current_cmd is None:
                    current_cmd = WineCMDAction(script=[], action="cmd")
                current_cmd.script.append(action)
            else:
                if current_cmd is not None:
                    yield current_cmd
                    current_cmd = None
                yield action
        if current_cmd is not None:
            yield current_cmd

    def install_dxvk_dll(self, env: MutableMapping[str, Any], name: str) -> None:
        wineprefix: Path = env["volumes"]["wineprefix"]
        windows_path = wineprefix / 'dosdevices' / 'c:' / 'windows'
        syswow64_path = windows_path / 'syswow64'
        system32_path = windows_path / 'system32'
        ok = False
        if syswow64_path.exists() or self.wine_arch == WineArch.WIN64:
            ok |= self.install_dxvk_platform_dll("x64", name, system32_path)
            if syswow64_path.exists():
                ok |= self.install_dxvk_platform_dll("x32", name, syswow64_path)
        else:
            ok |= self.install_dxvk_platform_dll("x32", name, system32_path)
        if ok:
            env.setdefault('wine_preinit_script', []).append(RegistryTemplate(template='registry', add={
                r"HKEY_CURRENT_USER\Software\Wine\DllOverrides": {
                    f"{name}": "native"
                }
            }))

    @staticmethod
    def uninstall_dxvk_dll(env: MutableMapping[str, Any], name: str) -> None:
        wineprefix: Path = env["volumes"]["wineprefix"]
        windows_path = wineprefix / 'dosdevices' / 'c:' / 'windows'
        syswow64_path = windows_path / 'syswow64'
        system32_path = windows_path / 'system32'
        if syswow64_path.exists():
            (syswow64_path / f"{name}.dll").unlink(missing_ok=True)
        (system32_path / f"{name}.dll").unlink(missing_ok=True)
        env.setdefault('wine_preinit_script', []).append(RegistryTemplate(template='registry', delete={
            r"HKEY_CURRENT_USER\Software\Wine\DllOverrides": [f"{name}"]
        }))

    def install_dxvk_platform_dll(self, platform: str, name: str, dest_dir: Path) -> bool:
        source = self.dxvk.dxvk_path / platform / f"{name}.dll.so"
        if not source.exists():
            source = self.dxvk.dxvk_path / platform / f"{name}.dll"
        if not source.exists():
            logger.warning(f"dxvk: {source} does not exist. Skipping installation in prefix.")
            return False
        target = dest_dir / f"{name}.dll"
        target.unlink(missing_ok=True)
        if self.dxvk.symlink:
            target.symlink_to(source)
        else:
            shutil.copy(source, target)
        return True

    def setup_dxvk(self, env: MutableMapping[str, Any]) -> None:
        exec_env = env["env"]
        if self.dxvk is not None and self.dxvk.enabled:
            if self.dxvk.force_uninstall:
                if self.dxvk.dxgi:
                    self.uninstall_dxvk_dll(env, 'dxgi')
                self.uninstall_dxvk_dll(env, 'd3d8')
                self.uninstall_dxvk_dll(env, 'd3d9')
                self.uninstall_dxvk_dll(env, 'd3d10core')
                self.uninstall_dxvk_dll(env, 'd3d11')
                # Recreate deleted dlls
                env.setdefault('wine_preinit_script', []).append(['wineboot',  '-u'])
            else:
                exec_env.setdefault("DXVK_STATE_CACHE", "1" if self.dxvk.state_cache else "0")
                if self.dxvk.state_cache_path is not None:
                    exec_env.setdefault("DXVK_STATE_CACHE_PATH",
                                        expand_single_argument(self.dxvk.state_cache_path, env))
                if self.dxvk.log_level is not None:
                    exec_env.setdefault("DXVK_LOG_LEVEL", expand_single_argument(self.dxvk.log_level, env))
                if self.dxvk.log_path is not None:
                    exec_env.setdefault("DXVK_LOG_PATH", expand_single_argument(self.dxvk.log_path, env))
                if self.dxvk.config_file is not None:
                    exec_env.setdefault("DXVK_CONFIG_FILE", expand_single_argument(self.dxvk.config_file, env))

                if self.dxvk.dxgi:
                    self.install_dxvk_dll(env, 'dxgi')
                self.install_dxvk_dll(env, 'd3d8')
                self.install_dxvk_dll(env, 'd3d9')
                self.install_dxvk_dll(env, 'd3d10core')
                self.install_dxvk_dll(env, 'd3d11')

    def volumes_ready(self, ep, env: MutableMapping[str, Any]):
        wineprefix: Path = env["volumes"]["wineprefix"]
        env["env"]["WINEPREFIX"] = str(wineprefix)
        dosdevices = wineprefix / "dosdevices"

        # Initialize prefix
        if not dosdevices.exists():
            WineAction(command=["wineboot", "-i"], action="wine").run(self, ep, env)
            # Wait for prefix initialisation
            WineServerAction(arguments="-w", action="wineserver").run(self, ep, env)

        # Setup mountpoints
        for drive, mountpoint in self.mount_points.items():
            volume = env["volumes"][mountpoint.volume]
            dosdevice = dosdevices / drive.drive.lower()
            if dosdevice.resolve() != volume.resolve():
                if dosdevice.is_symlink():
                    dosdevice.unlink()
                dosdevice.symlink_to(volume)
            # Remove hypothetic matching mount manager link that could cause
            # interference with our own system
            mm_link = dosdevices / f"{drive.drive.lower()}:"
            if mm_link.is_symlink():
                mm_link.unlink()

        self.setup_dxvk(env)
