# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

from contextlib import contextmanager
from pathlib import Path
from typing import Literal, Union, List, Any, MutableMapping, ContextManager, Iterable, Optional, Tuple

from ..actions import Action, SubProcessAction, CommonAction
from ..application import Application, EntryPoint
from ..argdsl import expand_single_argument
from ..features import WMClassFeature, Features, LDPreloadHelperFeature, LDPreloadHelper
from ..model import ExpandedPath

SUFFIXES: List[Tuple[str, str]] = [
    ("scare", ".taf"),
    ("advsys", ".dat"),
    ("agility", ".agx .d$$"),
    ("alan2", ".acd"),
    ("alan3", ".a3c"),
    ("geas", ".asl .cas"),
    ("git", ".ulx .blb .blorb .glb .gblorb"),
    ("hugo", ".hex"),
    ("jacl", ".jacl .j2"),
    ("level9", ".l9 .sna"),
    ("magnetic", ".mag"),
    ("tadsr", ".gam .t3"),
    ("bocfel", ".z1 .z2 .z3 .z4 .z5 .z6 .z7 .z8 .zlb .zblorb"),
]

INTERPRETERS = {}
for interp, suffixes in SUFFIXES:
    for suffix in suffixes.split(" "):
        INTERPRETERS[suffix] = interp
del SUFFIXES, interp, suffixes, suffix


class PlayAction(SubProcessAction):
    action: Literal["play"]
    fiction: Union[str, Path]
    interpreter: Optional[str] = None

    def resolve_interpreter(self, env) -> str:
        if self.interpreter is not None:
            return self.interpreter
        if isinstance(self.fiction, str):
            fiction = Path(expand_single_argument(self.fiction, env))
        else:
            fiction = self.fiction
        # noinspection PyShadowingNames
        suffix = fiction.suffix.lower()
        return INTERPRETERS[suffix]

    @contextmanager
    def yield_command(self, app: GargoyleApplication, ep, env) -> ContextManager[List[Any]]:
        # Don't use /usr/bin/gargoyle because it sometimes fails to pick up the right interpreter
        yield [f"{{interpreter_path}}/{self.resolve_interpreter(env)}", str(Path(expand_single_argument(self.fiction, env)).expanduser()), "{cmd_args}"]


GargoyleActionType = Union[CommonAction, PlayAction, str]


class GargoyleApplication(Application[GargoyleActionType, EntryPoint[GargoyleActionType]]):
    target: Literal['gargoyle']

    features: Features = Features(wm_class=WMClassFeature(),
                                  ldpreloadhelper=LDPreloadHelperFeature(helper=LDPreloadHelper.XCB))
    interpreter_path: ExpandedPath = Path("/usr/lib/gargoyle")

    def prepare_environment(self) -> MutableMapping[str, Any]:
        env = super().prepare_environment()
        env["interpreter_path"] = self.interpreter_path
        return env

    def transform_actions(self, actions, ep, env) -> Iterable[Action]:
        for action in actions:
            if isinstance(action, str):
                action = PlayAction(fiction=action, action="play")
            yield action
