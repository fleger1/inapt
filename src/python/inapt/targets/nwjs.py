# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

from contextlib import contextmanager
from pathlib import Path
from typing import Literal, Union, List, Any, MutableMapping, ContextManager, Iterable, Optional

from pydantic import BaseModel

from ..actions import Action, SubProcessAction, CommonAction
from ..application import Application, EntryPoint
from ..argdsl import expand_single_argument
from ..features import GTKFeature, WMClassFeature, Features
from ..game import GameRepositoryMixin, GameActionMixin
from ..model import ExpandedPath


class NWjsGame(BaseModel):
    nwapp: Optional[str] = None
    user_data_dir: Optional[Union[str, Path]] = None
    url: Optional[str] = None
    options: Optional[List[str]] = None


class NWjsAction(GameRepositoryMixin[NWjsGame],
                 GameActionMixin[NWjsGame]):
    pass


class RunAction(SubProcessAction, NWjsAction):
    action: Literal["run"]

    @contextmanager
    def yield_command(self, app: NWjsApplication, ep, env) -> ContextManager[List[Any]]:
        game = self.resolve_game(app, ep)
        cmd = ["{nw}"]
        # GTKFeature may have been disabled by the user
        if "gtk_args" in env:
            cmd.append("{gtk_args}")
        if game.options:
            cmd.extend(game.options)
        if game.nwapp:
            cmd.append(f"--nwapp={game.nwapp}")
        if game.user_data_dir:
            cmd.append(f"--user-data-dir={str(Path(expand_single_argument(game.user_data_dir, env)).expanduser())}")
        if game.url:
            cmd.append(f"--url={game.url}")
        cmd.append("{cmd_args}")
        yield cmd


NWjsActionType = Union[CommonAction, RunAction, str]


class NWjsEntryPoint(GameRepositoryMixin[NWjsGame],
                     EntryPoint[NWjsActionType]):
    pass


class NWjsApplication(GameRepositoryMixin[NWjsGame],
                      Application[NWjsActionType, NWjsEntryPoint]):
    target: Literal['nwjs']

    features: Features = Features(wm_class=WMClassFeature(), gtk=GTKFeature())
    nw: ExpandedPath = Path("nw")

    def prepare_environment(self) -> MutableMapping[str, Any]:
        env = super().prepare_environment()
        env["nw"] = self.nw
        return env

    def transform_actions(self, actions, ep, env) -> Iterable[Action]:
        for action in actions:
            if isinstance(action, str):
                action = RunAction(game=action, action="run")
            yield action
