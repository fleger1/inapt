# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

from abc import ABC
from contextlib import contextmanager
from pathlib import Path
from typing import Literal, Union, Mapping, List, Any, MutableMapping, ContextManager, Iterable, Optional

from pydantic import BaseModel

from ..actions import Action, SubProcessAction, CommonAction
from ..application import Application, EntryPoint
from ..argdsl import expand_single_argument
from ..features import WMClassFeature, SDLFeature, Features
from ..game import GameRepositoryMixin, GameActionMixin
from ..model import ExpandedPath


class CoreSearchPathOptionsMixin(BaseModel, ABC):
    homepath: Optional[Union[str, Path]] = None
    rompath: Optional[Union[str, Path]] = None
    hashpath: Optional[Union[str, Path]] = None
    samplepath: Optional[Union[str, Path]] = None
    artpath: Optional[Union[str, Path]] = None
    ctrlpath: Optional[Union[str, Path]] = None
    inipath: Optional[Union[str, Path]] = None
    fontpath: Optional[Union[str, Path]] = None
    cheatpath: Optional[Union[str, Path]] = None
    crosshairpath: Optional[Union[str, Path]] = None
    pluginspath: Optional[Union[str, Path]] = None
    languagepath: Optional[Union[str, Path]] = None
    swpath: Optional[Union[str, Path]] = None


class MameGame(CoreSearchPathOptionsMixin):
    machine: Optional[str] = None
    software: Optional[str] = None
    media: Optional[Mapping[str, Union[str, Path]]] = None
    options: Optional[List[str]] = None


class MameAction(GameActionMixin[MameGame],
                 GameRepositoryMixin[MameGame]):
    pass


class RunAction(SubProcessAction, MameAction):
    action: Literal["run"]

    @contextmanager
    def yield_command(self, app: MameApplication, ep, env) -> ContextManager[List[Any]]:
        game = self.resolve_game(app, ep)
        cmdline = ["{mame}"]
        if game.machine is not None:
            cmdline.append(game.machine)
        if game.software:
            cmdline.append(game.software)
        if game.media:
            for k, v in game.media.items():
                cmdline.append(f"-{k}")
                cmdline.append(str(Path(expand_single_argument(v, env)).expanduser()))

        for field in CoreSearchPathOptionsMixin.model_fields:
            val = getattr(game, field)
            if val is None:
                val = getattr(app, field)
            if val is not None:
                cmdline.append(f"-{field}")
                cmdline.append(str(Path(expand_single_argument(val, env)).expanduser()))

        if game.options:
            cmdline.extend(game.options)

        cmdline.append("{cmd_args}")

        yield cmdline


MameActionType = Union[CommonAction, RunAction, str]


class MameEntryPoint(GameRepositoryMixin[MameGame],
                     EntryPoint[MameActionType]):
    pass


class MameApplication(CoreSearchPathOptionsMixin,
                      GameRepositoryMixin[MameGame],
                      Application[MameActionType, MameEntryPoint]):
    target: Literal['mame']
    features: Features = Features(wm_class=WMClassFeature(), sdl=SDLFeature())
    mame: ExpandedPath = Path("mame")

    def prepare_environment(self) -> MutableMapping[str, Any]:
        env = super().prepare_environment()
        env["mame"] = self.mame
        return env

    def transform_actions(self, actions, ep, env) -> Iterable[Action]:
        for action in actions:
            if isinstance(action, str):
                action = RunAction(game=action, action="run")
            yield action
