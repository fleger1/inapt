# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

from collections import ChainMap
from contextlib import contextmanager
from dataclasses import dataclass
from pathlib import Path
from typing import Literal, Union, List, Any, MutableMapping, ContextManager, Iterable, Optional, Mapping

from pydantic import Field

from ..actions import Action, SubProcessAction, CommonAction
from ..application import Application, EntryPoint
from ..argdsl import expand_single_argument
from ..features import WMClassFeature, SDLFeature, Features, LDPreloadHelperFeature, LDPreloadHelper
from ..model import ExpandedPath
from ..volumes import XDGVolume, ENV_VOLUMES


class MGBAAction(SubProcessAction):
    action: Literal["mgba"]
    rom: Union[str, Path]
    bios: Optional[Union[str, Path]] = None
    cheats: Optional[Union[str, Path]] = None
    debug: bool = False
    gdb: bool = False
    log_level: Optional[int] = None
    savestate: Optional[Union[str, Path]] = None
    patch: Optional[Union[str, Path]] = None
    frameskip: Optional[int] = None
    config: Mapping[str, str] = Field(default_factory=dict)

    @contextmanager
    def yield_command(self, app: MGBAApplication, ep, env) -> ContextManager[List[Any]]:
        cmd = ["{mgba}", "{cmd_args}"]
        if self.bios is not None:
            cmd.extend(('--bios', str(Path(expand_single_argument(self.bios, env)).expanduser())))
        if self.cheats is not None:
            cmd.extend(('--cheats', str(Path(expand_single_argument(self.cheats, env)).expanduser())))
        if self.debug:
            cmd.append('-d')
        if self.gdb:
            cmd.append('-g')
        if self.log_level is not None:
            cmd.extend(('--log-level', str(self.log_level)))
        if self.savestate is not None:
            cmd.extend(('--savestate', str(Path(expand_single_argument(self.savestate, env)).expanduser())))
        if self.patch is not None:
            cmd.extend(('--patch', str(Path(expand_single_argument(self.patch, env)).expanduser())))
        if self.frameskip is not None:
            cmd.extend(('--frameskip', str(self.frameskip)))

        config = ChainMap({}, self.config)
        # Autofill configuration overrides for special volumes
        for special_volume in MGBA_SPECIAL_VOLUMES:
            if ENV_VOLUMES in env and special_volume.id in env[ENV_VOLUMES] \
                    and special_volume.config_param not in config:
                config[special_volume.config_param] = f"{{{ENV_VOLUMES}[{special_volume.id}]}}"

        for k, v in config.items():
            cmd.append('-C')
            cmd.append(f'{k}={v}')

        cmd.append(self.rom)

        yield cmd


MGBAActionType = Union[CommonAction, MGBAAction, str]


@dataclass
class MGBASpecialVolume:
    id: str
    config_param: str
    default_subdirectory: Optional[str] = None


# If these volumes are defined they will be automatically be mapped
# to the proper definitions
MGBA_SPECIAL_VOLUMES = (
    MGBASpecialVolume("mgba_savestate", "savestatePath", "savestate"),
    MGBASpecialVolume("mgba_screenshot", "screenshotPath", "screenshot"),
    MGBASpecialVolume("mgba_savegame", "savegamePath", "savegame"),
    MGBASpecialVolume("mgba_patch", "patchPath", "patch"),
    MGBASpecialVolume("mgba_cheats", "cheatsPath", "cheats"),
)


class MGBAApplication(Application[MGBAActionType, EntryPoint[MGBAActionType]]):
    target: Literal['mgba']

    # MGBA Frontend can be either SDL-based or qt-based
    # Let's enable both SDLFeature and LDPreloadHelperFeature to cover all our bases.
    features: Features = Features(wm_class=WMClassFeature(), sdl=SDLFeature(),
                                  ldpreloadhelper=LDPreloadHelperFeature(helper=LDPreloadHelper.XCB))

    # By default use the qt-based frontend
    mgba: ExpandedPath = Path("mgba-qt")

    def init(self):
        # Set up sane default values for storing user data under $XDG_DATA_HOME/mgba
        # We elect to use common directories for all roms to make it easier for the user to take advantage of mgba's
        # multiplayer feature.
        for special_volume in MGBA_SPECIAL_VOLUMES:
            if not special_volume.default_subdirectory:
                continue
            self.volumes.setdefault(special_volume.id, XDGVolume(volume="xdg",
                                                                 application='mgba',
                                                                 subdirectory=special_volume.default_subdirectory))

    def prepare_environment(self) -> MutableMapping[str, Any]:
        env = super().prepare_environment()
        env["mgba"] = self.mgba
        return env

    def transform_actions(self, actions, ep, env) -> Iterable[Action]:
        for action in actions:
            if isinstance(action, str):
                action = MGBAAction(rom=action, action="mgba")
            yield action
