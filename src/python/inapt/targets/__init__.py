# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from typing import Literal, Union, List, Iterable

from ..actions import ExecAction, Action, CommonAction
from ..application import Application, EntryPoint

GenericActionModelType = Union[CommonAction, List[str], str]


class GenericApplication(Application[GenericActionModelType, EntryPoint[GenericActionModelType]]):
    target: Literal["generic"]

    def transform_actions(self, actions, ep, env) -> Iterable[Action]:
        for action in actions:
            if isinstance(action, str) or isinstance(action, list):
                action = ExecAction(command=action, action="exec")
            yield action
