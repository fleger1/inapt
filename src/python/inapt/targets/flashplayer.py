# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

from contextlib import contextmanager
from pathlib import Path
from typing import Literal, Union, List, Any, MutableMapping, ContextManager, Iterable

from ..actions import Action, SubProcessAction, CommonAction
from ..application import Application, EntryPoint
from ..argdsl import expand_single_argument
from ..features import WMClassFeature, GTKFeature, Features
from ..model import ExpandedPath


class PlayAction(SubProcessAction):
    action: Literal["play"]
    swf: Union[str, Path]

    @contextmanager
    def yield_command(self, app: FlashPlayerApplication, ep, env) -> ContextManager[List[Any]]:
        cmd = ["{flashplayer}"]
        # GTKFeature may have been disabled by the user
        if "gtk_args" in env:
            cmd.append("{gtk_args}")
        cmd.append(str(Path(expand_single_argument(self.swf, env)).expanduser()))
        cmd.append("{cmd_args}")
        yield cmd


FlashPlayerActionType = Union[CommonAction, PlayAction, str]


class FlashPlayerApplication(Application[FlashPlayerActionType, EntryPoint[FlashPlayerActionType]]):
    target: Literal['flashplayer']

    features: Features = Features(wm_class=WMClassFeature(), gtk=GTKFeature())
    flashplayer: ExpandedPath = Path("flashplayer")

    def prepare_environment(self) -> MutableMapping[str, Any]:
        env = super().prepare_environment()
        env["flashplayer"] = self.flashplayer
        return env

    def transform_actions(self, actions, ep, env) -> Iterable[Action]:
        for action in actions:
            if isinstance(action, str):
                action = PlayAction(swf=action, action="play")
            yield action
