# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from abc import abstractmethod, ABC
from json import JSONEncoder, dump
from pathlib import Path
from typing import TextIO, Any, Type, Optional, Mapping, MutableMapping, Iterable, List


class FileGenerator(ABC):

    @abstractmethod
    def generate(self, file_path: Path) -> None:
        pass

    @property
    @abstractmethod
    def path(self) -> Path:
        pass


class CopyFileGenerator(FileGenerator):

    def __init__(self, source_file: Path, path: Path, file_mode: int = 0o644) -> None:
        self.source_file = source_file
        self.__path = path
        self.file_mode = file_mode

    def generate(self, file_path: Path) -> None:
        import shutil
        shutil.copyfile(self.source_file, file_path)
        file_path.chmod(self.file_mode)

    @property
    def path(self) -> Path:
        return self.__path


class TextBasedFileGenerator(FileGenerator, ABC):

    @property
    @abstractmethod
    def file_mode(self) -> int:
        pass

    def generate(self, file_path: Path) -> None:
        file_path.touch(self.file_mode, exist_ok=True)
        with file_path.open("w") as f:
            self.generate_text(f)

    @abstractmethod
    def generate_text(self, output_file: TextIO) -> None:
        pass


class TextFileGenerator(TextBasedFileGenerator):

    def __init__(self, content: str, path: Path, file_mode: int = 0o644) -> None:
        self.content = content
        self.__path = path
        self.__file_mode = file_mode

    @property
    def file_mode(self) -> int:
        return self.__file_mode

    def generate_text(self, output_file: TextIO) -> None:
        output_file.write(self.content)

    @property
    def path(self) -> Path:
        return self.__path


class JSONFileGenerator(TextBasedFileGenerator):

    def __init__(self, model_object: Any, path: Path, file_mode: int = 0o644,
                 cls: Optional[Type[JSONEncoder]] = None) -> None:
        self.model_object = model_object
        self.__path = path
        self.__file_mode = file_mode
        self.cls = cls

    @property
    def file_mode(self) -> int:
        return self.__file_mode

    def generate_text(self, output_file: TextIO) -> None:
        dump(self.model_object, output_file, cls=self.cls, indent=2)

    @property
    def path(self) -> Path:
        return self.__path


class IniFileGenerator(TextBasedFileGenerator):

    def __init__(self, model_object: Mapping, path: Path, file_mode: int = 0o644):
        self.model_object = model_object
        self.__path = path
        self.__file_mode = file_mode

    @property
    def file_mode(self) -> int:
        return self.__file_mode

    def generate_text(self, output_file: TextIO) -> None:
        from configparser import RawConfigParser, ConfigParser
        if isinstance(self.model_object, RawConfigParser):
            conf = self.model_object
        else:
            conf = ConfigParser()
            # Preserve case
            conf.optionxform = lambda option: option
            conf.update(self.model_object)
        conf.write(output_file)

    @property
    def path(self) -> Path:
        return self.__path


class SymlinkGenerator(FileGenerator):

    def __init__(self, path: Path, target: Path):
        self.__path = path
        self.__target = target

    def generate(self, file_path: Path) -> None:
        file_path.symlink_to(self.target)

    @property
    def path(self) -> Path:
        return self.__path

    @property
    def target(self) -> Path:
        return self.__target


class Installable(ABC):

    @abstractmethod
    def do_install(self, app: 'Application',
                   env: MutableMapping[str, Any],
                   props: Mapping[str, List[str]]) -> Iterable[FileGenerator]:
        yield from ()
