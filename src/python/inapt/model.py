# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path
from typing import Optional, Type, Union, Any, Mapping, Annotated

import pyparsing
from pydantic import AfterValidator

id_ok_chars = set(pyparsing.alphanums + "_-")


def id_validator(k: str) -> str:
    assert id_ok_chars >= set(k), f"{k} is not a valid id"
    return k


def id_key_validator(m: Mapping[str, Any]) -> Mapping[str, Any]:
    for k in m:
        id_validator(k)
    return m


IdStr = Annotated[str, AfterValidator(id_validator)]
ExpandedPath = Annotated[Path, AfterValidator(lambda p: p if p is None else p.expanduser())]

def _build_modelpart_grammar() -> pyparsing.Group:
    field_id = pyparsing.Word(''.join(id_ok_chars))
    index = pyparsing.Suppress('[') + pyparsing.Word(pyparsing.nums) + pyparsing.Suppress(']')
    modelpart = pyparsing.Group(field_id + pyparsing.Optional(index))
    return modelpart


@dataclass(eq=True, frozen=True)
class ModelPathPart:
    __grammar__ = _build_modelpart_grammar()

    id: str
    index: Optional[int] = None

    def __str__(self) -> str:
        if self.index is None:
            return self.id
        return f"{self.id}[{self.index}]"

    @classmethod
    def from_parse_results(cls: Type[ModelPathPart], p: pyparsing.ParseResults) -> ModelPathPart:
        args = [p[0]]
        if len(p) == 2:
            args.append(int(p[1]))
        return cls(*args)

    @classmethod
    def from_string(cls: Type[ModelPathPart], s: str) -> ModelPathPart:
        return cls.from_parse_results(cls.__grammar__.parseString(s))


ModelPathLike = Union['PureModelPath', ModelPathPart, str]


class PureModelPath(object):
    __grammar__ = pyparsing.delimitedList(ModelPathPart.__grammar__, '.')

    def __init__(self, path: Union[PureModelPath, ModelPathPart, pyparsing.ParseResults, str] = None) -> None:
        if isinstance(path, str):
            path = self.__grammar__.parseString(path)

        if path is None:
            self.parts = ()
        elif isinstance(path, PureModelPath):
            self.parts = tuple(path.parts)
        elif isinstance(path, ModelPathPart):
            self.parts = (path,)
        elif isinstance(path, pyparsing.ParseResults):
            self.parts = tuple(ModelPathPart.from_parse_results(t) for t in path)
        else:
            raise TypeError("Wrong type for path argument")

    @classmethod
    def _from_parts(cls, *parts):
        self = object.__new__(cls)
        self.parts = tuple(parts)
        return self

    def __str__(self):
        return '.'.join(str(p) for p in self.parts)

    def __repr__(self):
        return f"PureModelPath({self})"

    def __len__(self):
        return len(self.parts)

    def __eq__(self, other):
        return self.parts == other.parts

    def __hash__(self):
        return hash(self.parts)

    def __add__(self, other) -> PureModelPath:
        if not isinstance(other, PureModelPath):
            try:
                other = PureModelPath(other)
            except TypeError:
                return NotImplemented
        return self._from_parts(*self.parts, *other.parts)

    def __neg__(self) -> PureModelPath:
        if len(self.parts) == 0:
            return self
        return self._from_parts(*self.parts[:-1])

    def __getitem__(self, index: Optional[int] = None) -> PureModelPath:
        if len(self.parts) == 0:
            raise IndexError("Root is not indexable")
        indexed_head = ModelPathPart(self.parts[-1].id, index)
        return self._from_parts(*self.parts[:-1], indexed_head)
