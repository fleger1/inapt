# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from string import Formatter
from typing import Any, Mapping, List


class ArgumentFormatter(Formatter):

    def _vformat(self, format_string, args, kwargs, used_args, recursion_depth,
                 auto_arg_index=0):
        if recursion_depth < 0:
            raise ValueError('Max string recursion exceeded')
        results = [[]]
        for literal_text, field_name, format_spec, conversion in \
                self.parse(format_string):

            # output the literal text
            if literal_text:
                for result in results:
                    result.append(literal_text)

            # if there's a field, output it
            if field_name is not None:
                # this is some markup, find the object and do
                #  the formatting

                # handle arg indexing when empty field_names are given.
                if field_name == '':
                    if auto_arg_index is False:
                        raise ValueError('cannot switch from manual field '
                                         'specification to automatic field '
                                         'numbering')
                    field_name = str(auto_arg_index)
                    auto_arg_index += 1
                elif field_name.isdigit():
                    if auto_arg_index:
                        raise ValueError('cannot switch from manual field '
                                         'specification to automatic field '
                                         'numbering')
                    # disable auto arg incrementing, if it gets
                    # used later on, then an exception will be raised
                    auto_arg_index = False

                # given the field_name, find the object it references
                #  and the argument it came from
                obj, arg_used = self.get_field(field_name, args, kwargs)
                used_args.add(arg_used)

                # do any conversion on the resulting object
                obj = self.convert_field(obj, conversion)

                # expand the format spec, if needed
                format_spec, auto_arg_index = self._vformat(
                    format_spec, args, kwargs,
                    used_args, recursion_depth - 1,
                    auto_arg_index=auto_arg_index)
                format_spec = format_spec[0]

                if isinstance(obj, list) or isinstance(obj, set) or isinstance(obj, tuple):
                    if len(obj) == 0:
                        return None, auto_arg_index
                    if len(obj) == 1:
                        f = self.format_field(next(iter(obj)), format_spec)
                        for result in results:
                            result.append(f)
                    else:
                        new_results = []
                        for result in results:
                            r = ''.join(result)
                            for o in obj:
                                f = self.format_field(o, format_spec)
                                new_results.append([r, f])
                        results = new_results
                else:
                    f = self.format_field(obj, format_spec)
                    for result in results:
                        result.append(f)

        return [''.join(result) for result in results], auto_arg_index


def expand_commandline(cmd: List[Any], env: Mapping) -> List[Any]:
    expanded_cmd = []
    formatter = ArgumentFormatter()
    for arg in cmd:
        if isinstance(arg, str):
            expanded_arg = formatter.vformat(arg, (), env)
            if expanded_arg:
                expanded_cmd.extend(expanded_arg)
        else:
            expanded_cmd.append(arg)
    return expanded_cmd


def expand_single_argument(arg: Any, env: Mapping) -> Any:
    if not isinstance(arg, str):
        return arg
    args = ArgumentFormatter().vformat(arg, (), env)
    if args is None or len(args) == 0:
        raise ValueError(f"{arg} expands to nothing")
    elif len(args) != 1:
        raise ValueError(f"{arg} expands to more than one value")
    return args[0]
