# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import logging
import sys
from pathlib import Path
from typing import Any

from pydantic import ValidationError

from .application import EntryPointNotFoundException
from .config import load_model


def cli(*argv) -> Any:
    if len(argv) < 2:
        return f"Usage: {sys.argv[0]} CONFIG ENTRY_POINT [ARGS...]"

    logging.basicConfig(level=logging.DEBUG)

    yaml_app = Path(argv[0])
    entry_point = argv[1]

    if not yaml_app.is_file():
        return f"{yaml_app} is not a file"

    extension_d = yaml_app.parent / f"{yaml_app.stem}.d"
    yaml_extensions = filter(lambda path: path.is_file, extension_d.glob("*.yaml")) if extension_d.is_dir() else None

    try:
        root = load_model(yaml_app, yaml_extensions)
        try:
            return root.application.run(entry_point, *argv[2:])
        except EntryPointNotFoundException as e:
            return str(e)
    except ValidationError as e:
        return str(e)


def main() -> None:
    sys.exit(cli(*sys.argv[1:]))