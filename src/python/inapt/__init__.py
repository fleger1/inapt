# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import logging
import os
import sys

from . import _version
__version__ = _version.get_versions()['version']

# Logging setup
log_level = os.getenv("INAPT_LOGLEVEL")
if log_level is not None:
    try:
        log_level = int(log_level)
    except ValueError:
        log_level = log_level.upper()
    try:
        logging.basicConfig(level=log_level)
    except TypeError as e:
        # Invalid log level (non fatal but print error nonetheless)
        print(e, file=sys.stderr)
del logging, os, sys


class InaptException(Exception):
    pass


if __name__ == "__main__":
    from .cli import main
    main()
