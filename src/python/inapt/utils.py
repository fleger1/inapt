# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import logging
from types import UnionType
from typing import Type, Union, Any, Optional, Annotated, get_origin

from pkg_resources import iter_entry_points
from pydantic import BaseModel, create_model, Field

logger = logging.getLogger(__name__)


def union_from_entry_points(group: str, *additional_types) -> Type[Union[Any]]:
    registry = []
    for ep in iter_entry_points(group):
        model = ep.load()
        registry.append(model)
    if additional_types:
        registry.extend(additional_types)
    typ = Union[tuple(registry)]
    logger.debug(f"Type for {group} is {typ}")
    return typ


def model_from_entry_points(model_name: str, group: str) -> Type[BaseModel]:
    return create_model(model_name, **{
        ep.name: (Optional[ep.load()], None) for ep in iter_entry_points(group)
    })


def with_discriminator(type_: Any, discriminator: str):
    """
    Annotate a type with a pydantic discriminator.
    """
    if get_origin(type_) in (Union, UnionType):
        return Annotated[type_, Field(discriminator=discriminator)]
    # Discriminated unions cannot be used with only a single variant
    # Python changes Union[T] into T at interpretation time,
    # so it is not possible for pydantic to distinguish fields of Union[T] from T.
    # In this case do not annotate the type.
    return type_
