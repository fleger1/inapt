# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import functools
import logging
import os
import subprocess
import sys
from abc import ABC, abstractmethod
from collections import ChainMap
from contextlib import contextmanager
from dataclasses import dataclass
from pathlib import Path
from subprocess import CompletedProcess
from typing import Union, List, Literal, Mapping, Any, MutableMapping, ContextManager, Optional, Sequence, TypeVar, \
    Generic, FrozenSet

from pydantic import BaseModel, Field

from .argdsl import expand_single_argument, expand_commandline
from .model import IdStr
from .scripting import ScriptActionMixin
from .utils import with_discriminator, union_from_entry_points

logger = logging.getLogger(__name__)

AR = TypeVar("AR")


class Action(BaseModel, Generic[AR], ABC):
    """
    Base class for actions.

    Subclasses *must* define an action string literal
    in order to be identified unambiguously in application
    definition files.

    Actions that are common to all targets should be furthermore
    declared as an `inapt.actions.common` distutils entry point.
    """
    action: str

    @abstractmethod
    def run(self, app, ep, env: MutableMapping) -> AR:
        """
        Method to be implemented to run the action.

        :param app: the current application
        :type app: :class:`inapt.application.Application`
        :param ep: the current entry point
        :type ep: :class:`inapt.application.EntryPoint`
        :param env: the current environnement
        :return: result of the action (not used for now).
        """
        pass


class ResourceLimits(BaseModel):
    core: Optional[int] = None
    cpu: Optional[int] = None
    fsize: Optional[int] = None
    data: Optional[int] = None
    stack: Optional[int] = None
    rss: Optional[int] = None
    nproc: Optional[int] = None
    nofile: Optional[int] = None
    ofile: Optional[int] = None
    memlock: Optional[int] = None
    vmem: Optional[int] = None
    as_: Optional[int] = Field(None, alias='as')
    msgqueue: Optional[int] = None
    nice: Optional[int] = None
    rtprio: Optional[int] = None
    rttime: Optional[int] = None
    sigpending: Optional[int] = None
    sbsize: Optional[int] = None
    swap: Optional[int] = None
    npts: Optional[int] = None
    kqueues: Optional[int] = None


@dataclass
class PreExecParams:
    cpu_affinity: Optional[FrozenSet[int]] = None
    resource_limits: Optional[ResourceLimits] = None


class SubProcessAction(Action[CompletedProcess], ABC):
    """
    Base class for actions that spawn an external subprocess.

    Subclasses shall implement the yield_command method.
    """
    cwd: Optional[str] = None
    cpu_affinity: Optional[List[int]] = None
    shell: bool = False
    resource_limits: Optional[ResourceLimits] = None

    @staticmethod
    def get_wrappers_from_env(env: MutableMapping) -> List[str]:
        return env.setdefault("subprocess_wrappers", [])

    def copy_preexec_params(self) -> PreExecParams:
        """
        Makes a copy of the parameters used in the preexec_fn hook.

        A copy is made before forking to guard against concurrent modifications.
        """
        return PreExecParams(
            frozenset(self.cpu_affinity) if self.cpu_affinity is not None else None,
            self.resource_limits.model_copy() if self.resource_limits is not None else None
        )

    @staticmethod
    def preexec_fn(preexec_params: PreExecParams) -> None:
        """
        Function called after forking a new interpreter on the child process.
        """
        if preexec_params.resource_limits is not None:
            try:
                import resource
            except ImportError:
                resource = None
            if resource is not None:
                for k, soft_limit in preexec_params.resource_limits.model_dump(by_alias=True).items():
                    if soft_limit is not None:
                        res_varname = f'RLIMIT_{k.upper()}'
                        try:
                            res = getattr(resource, res_varname)
                            hard_limit = resource.getrlimit(res)[1]
                            resource.setrlimit(res, (soft_limit, hard_limit))
                        except AttributeError:
                            pass

        if preexec_params.cpu_affinity:
            os.sched_setaffinity(0, preexec_params.cpu_affinity)

    def run(self, app, ep, env: MutableMapping) -> Optional[CompletedProcess]:
        with self.yield_command(app, ep, env) as command:
            if command is not None:
                command = self.get_wrappers_from_env(env) + command
                command = expand_commandline(command, env)

                run_args = {"preexec_fn": functools.partial(self.preexec_fn, self.copy_preexec_params())}

                cwd = expand_single_argument(self.cwd, env)
                if cwd is not None:
                    run_args["cwd"] = cwd

                if "env" in env:
                    run_args["env"] = env["env"]

                run_args["shell"] = self.shell

                logger.debug(f"Running {command}")
                comp = subprocess.run(command, **run_args)
            else:
                comp = None
        return comp

    @abstractmethod
    def yield_command(self, app, ep, env: MutableMapping) -> ContextManager[Optional[List[Any]]]:
        yield from ()


class TempScriptAction(ScriptActionMixin[Union[str, List[str]]],
                       SubProcessAction):
    action: Literal['temp_script']
    args: List[str] = []
    shell_escape: bool = True

    def join(self, items: Sequence[str]) -> str:
        if self.shell_escape:
            import shlex
            return shlex.join(items)
        return super().join(items)

    @contextmanager
    def yield_command(self, app, ep, env: MutableMapping) -> ContextManager[List[Any]]:
        from xdg import BaseDirectory
        from tempfile import NamedTemporaryFile
        with NamedTemporaryFile(mode="w", dir=BaseDirectory.get_runtime_dir(strict=False), prefix=app.id,
                                delete=False) as script:
            script.writelines(f"{line}\n" for line in self.process_script(app, ep, env, self.script))
        script_path = Path(script.name)
        logger.info(f"Temp script file generated as {script_path}")
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"Temp script content:\n{script_path.read_text()}")
        try:
            script_path.chmod(0o755)
            yield [str(script_path.absolute())] + self.args
        finally:
            script_path.unlink(missing_ok=True)


class ExecAction(SubProcessAction):
    action: Literal["exec"]
    command: Union[str, List[str]]

    @contextmanager
    def yield_command(self, app, ep, env) -> ContextManager[List[str]]:
        import shlex
        yield shlex.split(self.command) if isinstance(self.command, str) else self.command


class ShellAction(SubProcessAction):
    action: Literal["shell"]
    shell: Optional[Path] = None

    @contextmanager
    def yield_command(self, app, ep, env: MutableMapping) -> ContextManager[List[Any]]:
        if self.shell is None:
            unix_shell = ('/system/bin/sh' if
                          hasattr(sys, 'getandroidapilevel') else '/bin/sh')
        else:
            unix_shell = str(self.shell)
        yield [unix_shell]


class RefAction(Action[List[Any]]):
    action: Literal["ref"]
    ref: IdStr
    args: Mapping[str, Any] = {}

    def run(self, app, ep, env: MutableMapping) -> List[Any]:
        actions = app.actions[self.ref]
        env["ref_args"] = {k: expand_single_argument(v, env) for k, v in self.args.items()}
        ret = []
        for action in app.transform_actions(actions, ep, env):
            action_env = ChainMap({}, env)
            ret.append(action.run(app, ep, action_env))
        return ret


CommonAction = with_discriminator(union_from_entry_points("inapt.actions.common"), 'action')
