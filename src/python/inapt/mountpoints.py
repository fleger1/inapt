# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from abc import ABC
from typing import TypeVar, Generic, Mapping

from pydantic import BaseModel


class MountPoint(BaseModel, ABC):
    volume: str


KMP = TypeVar("KMP")
VMP = TypeVar("VMP", bound=MountPoint)


class MountPointRepositoryMixin(BaseModel, Generic[KMP, VMP]):
    mount_points: Mapping[KMP, VMP] = {}