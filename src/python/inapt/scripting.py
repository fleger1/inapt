# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

from abc import ABC, abstractmethod
from collections import ChainMap
from typing import Iterable, TypeVar, Generic, List, Sequence, Mapping, Literal, Any

from pkg_resources import iter_entry_points
from pydantic import BaseModel

from .argdsl import expand_single_argument, expand_commandline
from .model import IdStr
from .utils import union_from_entry_points, with_discriminator


class ScriptTemplate(BaseModel, ABC):
    template: str

    @abstractmethod
    def lines(self, app, ep, env, action: ScriptActionMixin) -> Iterable[str]:
        yield from ()


class ScriptRefTemplate(ScriptTemplate):
    template: Literal["script_ref"]
    ref: IdStr
    args: Mapping[str, Any] = {}

    def lines(self, app, ep, env, action: ScriptActionMixin) -> Iterable[str]:
        repositories = []
        for candidate in (action, ep, app):
            if isinstance(candidate, ScriptRepositoryMixin):
                repositories.append(candidate.scripts)
        repository = ChainMap(*repositories)
        script = repository[self.ref]
        new_env = ChainMap({}, env)
        new_env["script_ref_args"] = {k: expand_single_argument(v, env) for k, v in self.args.items()}
        yield from action.process_script(app, ep, new_env, script)


SC = TypeVar("SC")


class ScriptActionMixin(BaseModel, Generic[SC], ABC):
    script: List[SC]

    def process_string(self, app, ep, env, line: str) -> str:
        return expand_single_argument(line, env)

    def process_stringlist(self, app, ep, env, line: List[str]) -> str:
        expanded_line = expand_commandline(line, env)
        return self.join(expanded_line)

    def join(self, items: Sequence[str]) -> str:
        return ' '.join(items)

    def process_script(self, app, ep, env, content: Iterable[SC]) -> Iterable[str]:
        for line in content:
            if isinstance(line, str):
                yield self.process_string(app, ep, env, line)
            elif isinstance(line, List):
                yield self.process_stringlist(app, ep, env, line)
            elif isinstance(line, ScriptTemplate):
                yield from line.lines(app, ep, env, self)


class ScriptRepositoryMixin(BaseModel, Generic[SC]):
    scripts: Mapping[IdStr, List[SC]] = {}


CommonScriptTemplate = with_discriminator(union_from_entry_points("inapt.scripting.common"), 'template')


def is_common_script_template(obj: Any) -> bool:
    for ep in iter_entry_points("inapt.scripting.common"):
        model = ep.load()
        if isinstance(obj, model):
            return True
    return False
