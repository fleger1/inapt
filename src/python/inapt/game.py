# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

from collections import ChainMap
from typing import TypeVar, Mapping, Generic, Union

from pydantic import BaseModel

from inapt.model import IdStr

GAME = TypeVar("GAME")


class GameActionMixin(BaseModel, Generic[GAME]):
    game: Union[GAME, str]

    def resolve_game(self, app, ep) -> GAME:
        if isinstance(self.game, str):
            repositories = []
            for candidate in (self, ep, app):
                if isinstance(candidate, GameRepositoryMixin):
                    repositories.append(candidate.games)
            repository = ChainMap(*repositories)
            return repository[self.game]
        return self.game


class GameRepositoryMixin(BaseModel, Generic[GAME]):
    games: Mapping[IdStr, GAME] = {}
