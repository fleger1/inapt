# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import logging
from pathlib import Path
from typing import Union, Dict, Hashable, Any, Mapping, Iterable, Optional, Set, Tuple

import yaml

from .application import Root, RootExtension

logger = logging.getLogger(__name__)


def to_path(path: Union[str, Path]) -> Path:
    return path if isinstance(path, Path) else Path(path)


def load_yaml_file(yaml_file: Union[str, Path]) -> Union[Dict[Hashable, Any], list, None]:
    with to_path(yaml_file).open() as stream:
        return yaml.safe_load(stream)


def merge_extension(application: Union[Dict[Hashable, Any], list, None], extension: Mapping[str, Any]) -> None:
    if not isinstance(application, dict) or "application" not in application:
        return

    app_dict = application["application"]

    if not isinstance(app_dict, dict):
        return

    # Cycle prevention
    processed_dict_ids: Set[Tuple[int, int]] = set()

    def merge_dicts(d1: Dict[Hashable, Any], d2: Mapping[Hashable, Any]) -> None:
        ids = (id(d1), id(d2))
        if ids in processed_dict_ids:
            return
        processed_dict_ids.add(ids)
        for k, v in d2.items():
            if k in d1:
                v1 = d1[k]
                if isinstance(v, dict) and isinstance(v1, dict):
                    merge_dicts(v1, v)
                else:
                    d1[k] = v
            else:
                d1[k] = v

    merge_dicts(app_dict, extension)


def load_model(yaml_app: Union[str, Path],
               yaml_extensions: Optional[Iterable[Union[str, Path]]] = None) -> Root:
    yaml_app = to_path(yaml_app)
    extension_path = {}
    extension_models = []
    if yaml_extensions is not None:
        for f in yaml_extensions:
            f = to_path(f)
            logger.debug(f"Loading extension definition {f}")
            extension = RootExtension.model_validate(load_yaml_file(f)).extension
            if extension.id in extension_path:
                raise KeyError(f"{f} and {extension_path[extension.id]} are both identified by {extension.id}")
            extension_path[extension.id] = f
            extension_models.append(extension)
    extension_models.sort(key=lambda x: (x.priority, x.id))
    logger.debug(f"Loading application definition {yaml_app}")
    application_dict = load_yaml_file(yaml_app)
    for extension_model in extension_models:
        logger.debug(f"Applying extension {extension_model.id} (priority: {extension_model.priority})")
        merge_extension(application_dict, extension_model.application)
    if (app_dict := application_dict.get("application", None)) is not None:
        app_dict["yaml"] = yaml_app
        app_dict["yaml_ext"] = extension_path
    return Root.model_validate(application_dict)


if __name__ == "__main__":
    import sys
    try:
        # noinspection PyPackageRequirements
        from devtools import debug
    except ImportError:
        from pprint import pprint as debug
    debug(load_model(sys.argv[1], sys.argv[2:]))
