# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import logging
from abc import ABC
from enum import Enum
from pathlib import Path
from typing import Optional, MutableMapping, Dict, List, Union, Mapping, ClassVar

from pydantic import BaseModel, Field

from .actions import SubProcessAction
from .argdsl import expand_single_argument
from .utils import model_from_entry_points

logger = logging.getLogger(__name__)


class Feature(BaseModel, ABC):
    enabled: bool = True

    def init(self, app, ep, env: MutableMapping) -> None:
        pass

    def setup(self, app, ep, env: MutableMapping) -> None:
        pass


class WMClassFeature(Feature):
    wm_class: Optional[str] = None

    def init(self, app, ep, env: MutableMapping):
        super().init(app, ep, env)
        wm_class = self.wm_class
        if not wm_class and ep:
            wm_class = ep.desktop_entry
        if not wm_class:
            wm_class = app.desktop_entry
        env["wm_class"] = expand_single_argument(wm_class, env)


class SDLFeature(Feature):
    ENV_PROP: ClassVar[str] = 'SDL_VIDEO_X11_WMCLASS'

    def setup(self, app, ep, env: MutableMapping):
        super().setup(app, ep, env)
        if "wm_class" in env:
            wm_class = env["wm_class"]
            exec_env: Dict[str, str] = env["env"]
            if wm_class and self.ENV_PROP not in exec_env:
                exec_env[self.ENV_PROP] = wm_class
                logger.debug(f"{self.ENV_PROP} set to {wm_class}")


class LDPreloadHelper(str, Enum):
    X11 = "x11"
    XCB = "xcb"


class LDPreloadHelperFeature(Feature):
    ENV_PROP: ClassVar[str] = 'INAPT_WMCLASS'
    helper: LDPreloadHelper = LDPreloadHelper.X11

    def setup(self, app, ep, env: MutableMapping):
        super().setup(app, ep, env)
        import pkg_resources
        helpers_path = Path(pkg_resources.resource_filename("inapt", "helpers")).absolute()
        exec_env: Dict[str, str] = env["env"]

        ld_preload = exec_env.get("LD_PRELOAD", "")
        if ld_preload:
            ld_preload += ":"
        ld_preload += str(helpers_path / '$PLATFORM' / f'lib{self.helper.value}helper.so')
        exec_env["LD_PRELOAD"] = ld_preload
        logger.debug(f"LD_PRELOAD set to {ld_preload}")

        if "wm_class" in env:
            wm_class = env["wm_class"]
            if wm_class and self.ENV_PROP not in exec_env:
                exec_env[self.ENV_PROP] = wm_class
                logger.debug(f"{self.ENV_PROP} set to {wm_class}")


class GTKFeature(Feature):

    def setup(self, app, ep, env: MutableMapping):
        super().setup(app, ep, env)
        gtk_args = []
        if "wm_class" in env:
            wm_class = env["wm_class"]
            if wm_class:
                gtk_args.append(f"--class={wm_class}")
        env["gtk_args"] = gtk_args
        logger.debug(f"gtk_args set to {gtk_args}")


class DiscreteGPUFeature(Feature):
    env: Mapping[str, str] = Field(default_factory=lambda: {
        "_NV_PRIME_RENDER_OFFLOAD": "1",
        "__VK_LAYER_NV_optimus": "NVIDIA_only",
        "__GLX_VENDOR_LIBRARY_NAME": "nvidia",
        "DRI_PRIME": "1"
    })

    wrappers: List[Union[str, List[str]]] = Field(default_factory=lambda: [
        'primusrun',
        'optirun'
    ])

    def find_wrapper(self) -> Optional[List[str]]:
        """
        First existing wrapper.
        """
        import shutil
        for wrapper_cmd in self.wrappers:
            if isinstance(wrapper_cmd, str):
                wrapper_cmd = [wrapper_cmd]
            wrapper = shutil.which(wrapper_cmd[0])
            if wrapper is not None:
                # Resolve absolute path now because we may change the cwd later when we execute the command.
                wrapper = str(Path(wrapper).absolute())
                return [wrapper, *wrapper_cmd[1:]]

    def setup(self, app, ep, env: MutableMapping):
        super().init(app, ep, env)
        wrapper = self.find_wrapper()
        if wrapper:
            # If a wrapper is found, use it.
            SubProcessAction.get_wrappers_from_env(env).extend(wrapper)
        else:
            # Otherwise set a bunch of environment variables
            exec_env: Dict[str, str] = env["env"]
            for k, v in self.env.items():
                exec_env.setdefault(k, v)


Features = model_from_entry_points("Features", "inapt.features")
