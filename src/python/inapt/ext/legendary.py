# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

import logging
import os
from contextlib import contextmanager
from pathlib import Path
from typing import MutableMapping, ContextManager, Literal, Optional

from .. import InaptException
from ..argdsl import expand_single_argument
from ..features import Feature
from ..model import ExpandedPath
from ..volumes import Volume

logger = logging.getLogger(__name__)


class InaptLegendaryException(InaptException):
    pass


class LegendaryFeature(Feature):
    """
    Enable integration of Epic Launcher Games using Legendary.
    """
    app_name: str
    app_path: str
    offline: bool = False
    download: bool = False

    def init(self, app, ep, env: MutableMapping) -> None:
        super().init(app, ep, env)
        try:
            from legendary.core import LegendaryCore
        except ImportError:
            raise InaptLegendaryException(f'Please install Legendary in order to use the legendary feature!')
        core = LegendaryCore()
        env["legendary"] = {"core": core}
        try:
            if not core.login():
                raise InaptLegendaryException(f'Log in failed!')
        except ValueError:
            raise InaptLegendaryException('No credentials stored! '
                                          'Please login with legendary before running this game!')

    @staticmethod
    def download_game(core, app_name, game, dest_path: Path):
        if game.is_dlc:
            app_name = game.metadata['mainGameItem']['releaseInfo'][0]['appId']
            base_game = core.get_game(app_name)
        else:
            base_game = None
        dlm, analysis, igame = core.prepare_download(game=game,
                                                     base_game=base_game,
                                                     base_path=str(dest_path.parent),
                                                     game_folder=str(dest_path.name))
        if not analysis.dl_size:
            logger.info('Download size is 0, the game is either already up to date or has not changed.')
            return igame

        res = core.check_installation_conditions(analysis=analysis, install=igame, game=game,
                                                 updating=core.is_installed(app_name))

        if res.warnings or res.failures:
            logger.info('Installation requirements check returned the following results:')

        if res.warnings:
            for warn in sorted(res.warnings):
                logger.warning(warn)

        if res.failures:
            for msg in sorted(res.failures):
                logger.fatal(msg)
            logger.error('Installation cannot proceed, exiting.')
            raise InaptLegendaryException("Installation Failure")

        try:
            dlm.start()
            dlm.join()
        except Exception:
            raise InaptLegendaryException("Installation failure")

        return igame

    def get_game(self, env):
        app_name = expand_single_argument(self.app_name, env)
        app_path = expand_single_argument(self.app_path, env)
        app_path = os.path.abspath(app_path)
        core = env["legendary"]["core"]

        if core.is_installed(app_name):
            # Game already installed
            return app_name

        game = core.get_game(app_name, update_meta=True)
        if not game:
            raise InaptLegendaryException(f'Did not find game "{app_name}" on account.')

        if self.download:
            igame = self.download_game(core, app_name, game, Path(app_path))
            core.install_game(igame)

            old_igame = core.get_installed_game(game.app_name)
            if old_igame and old_igame.install_tags != igame.install_tags:
                old_igame.install_tags = igame.install_tags
                logger.info('Deleting now untagged files.')
                core.uninstall_tag(old_igame)
                core.install_game(old_igame)
        else:
            if not os.path.exists(app_path):
                raise InaptLegendaryException(f'Specified path "{app_path}" does not exist!')
            _, igame = core.import_game(game, app_path)
            core.install_game(igame)
        return app_name

    def setup(self, app, ep, env: MutableMapping):
        super().setup(app, ep, env)
        app_name = self.get_game(env)
        core = env["legendary"]["core"]
        offline = core.is_offline_game(app_name) or self.offline
        if not offline:
            try:
                if not core.login():
                    raise InaptLegendaryException(f'Log in failed!')
            except ValueError:
                raise InaptLegendaryException('No credentials stored! '
                                              'Please login with legendary before running this game!')
        launch_params = core.get_launch_parameters(app_name=app_name, offline=offline, disable_wine=True)
        try:
            # Legendary < 0.20.11
            params, cwd, env_leg = launch_params
        except TypeError:
            # Legendary >= 0.20.11
            params = []
            params.extend(launch_params.launch_command)
            params.append(os.path.join(launch_params.game_directory, launch_params.game_executable))
            params.extend(launch_params.game_parameters)
            params.extend(launch_params.egl_parameters)
            params.extend(launch_params.user_parameters)
            cwd = launch_params.working_directory
            env_leg = launch_params.environment

        env["legendary"]["params"] = params
        env["legendary"]["cwd"] = cwd
        for k, v in env_leg.items():
            if k not in env["env"]:
                env["env"][k] = v


class LegendaryVolume(Volume):
    volume: Literal["legendary"]
    base_path: Optional[ExpandedPath] = None

    @contextmanager
    def _setup(self: LegendaryVolume, app, ep, env: MutableMapping) -> ContextManager[LegendaryVolume]:
        try:
            core = env["legendary"]["core"]
        except KeyError:
            raise InaptLegendaryException("Legendary Core not initialized. Have you enabled the Legendary feature?")
        app_name = expand_single_argument(app.features.legendary.app_name, env)
        game = core.get_game(app_name, update_meta=True)
        if game.is_dlc:
            app_name = game.metadata['mainGameItem']['releaseInfo'][0]['appId']
            base_game = core.get_game(app_name)
        else:
            base_game = game
        if core.is_installed(base_game.app_name):
            igame = core.get_installed_game(base_game.app_name)
            install_path = Path(igame.install_path)
        else:
            game_folder = base_game.metadata.get('customAttributes', {}). \
                get('FolderName', {}).get('value', base_game.app_name)
            base_path = self.base_path or Path(core.get_default_install_dir())
            base_path.mkdir(parents=True, exist_ok=True)
            install_path = base_path / game_folder

        with self._register_volume(env, install_path) as volume:
            yield volume
