# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

import sys
from argparse import ArgumentParser
from collections import defaultdict
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Type

from pydantic import ValidationError
from pyparsing import Word, Suppress, Group, alphanums, originalTextFor, CharsNotIn, ParseResults

from . import __version__
from .config import load_model
from .installable import FileGenerator
from .model import PureModelPath


def _build_define_grammar():
    column = ':'
    equals = '='
    prop = Word(alphanums + '_')
    val = originalTextFor(CharsNotIn(''))
    return Group(PureModelPath.__grammar__) + Suppress(column) + prop + Suppress(equals) + val


@dataclass(frozen=True, eq=True)
class Define:
    __grammar__ = _build_define_grammar()

    path: PureModelPath
    property: str
    value: str

    def __str__(self) -> str:
        return f"{self.path}:{self.property}={self.value}"

    def __repr__(self) -> str:
        return f"-D{self}"

    @classmethod
    def from_parse_results(cls: Type[Define], p: ParseResults) -> Define:
        return cls(PureModelPath(p[0]), p[1], p[2])

    @classmethod
    def from_string(cls: Type[Define], s: str) -> Define:
        return cls.from_parse_results(cls.__grammar__.parseString(s))


del _build_define_grammar


def build_parser() -> ArgumentParser:
    parser = ArgumentParser(description="Inapt Installation Tool")
    parser.add_argument("--version", "-v", action="version", version=f"%(prog)s {__version__}")
    parser.add_argument("--root", "-r", type=Path, required=False, default=Path("/"),
                        help='Install everything relative to this alternate root directory')
    parser.add_argument("--define", "-D", type=Define.from_string,
                        metavar='ID:PROPERTY=VALUE', action="append",
                        help='Define installation property')
    parser.add_argument("--exclude", "-e", metavar='ID',
                        type=PureModelPath, action="append", help='Define installation property')
    parser.add_argument("--extension", "-E", type=Path, action="append", help="Load additional extension")
    parser.add_argument("application", metavar='APPLICATION', type=Path)
    parser.add_argument("ids", metavar='ID', type=PureModelPath, nargs='*')
    return parser


def generate_file(file_generator: FileGenerator, root: Path) -> Path:
    file_path = root / file_generator.path.relative_to(file_generator.path.root)
    print(f"Generating {file_path}...")
    file_path.parent.mkdir(parents=True, exist_ok=True)
    file_generator.generate(file_path)
    return file_path


def cli(*argv) -> Any:
    parser = build_parser()
    ns = parser.parse_args(argv)

    if not ns.application.is_file():
        return f"{ns.application} is not a file"

    if ns.extension:
        for e in ns.extension:
            if not e.is_file():
                return f"{e} is not a file"
    try:
        root = load_model(ns.application, ns.extension)
    except ValidationError as e:
        return str(e)

    properties = defaultdict(dict)

    if ns.define:
        for d in ns.define:
            properties[d.path].setdefault(d.property, []).append(d.value)

    for file_gen in root.application.install(set(ns.ids if ns.ids is not None else ()),
                                             set(ns.exclude if ns.exclude is not None else ()),
                                             properties):
        generate_file(file_gen, ns.root)


def main() -> None:
    sys.exit(cli(*sys.argv[1:]))


if __name__ == "__main__":
    main()
