# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

import logging
import os
from abc import ABC
from collections import ChainMap
from contextlib import ExitStack
from pathlib import Path
from typing import Optional, Generic, List, Mapping, TypeVar, Any, MutableMapping, Iterable, Set, DefaultDict

from pydantic import BaseModel, FilePath, GetCoreSchemaHandler
from pydantic_core import CoreSchema, core_schema

from . import InaptException
from .actions import Action
from .argdsl import expand_single_argument
from .features import Features
from .installable import FileGenerator, CopyFileGenerator, TextFileGenerator, Installable
from .model import PureModelPath, ExpandedPath, IdStr
from .utils import union_from_entry_points, with_discriminator
from .volumes import ENV_VOLUME_ID

logger = logging.getLogger(__name__)

AC = TypeVar("AC")


# see https://stackoverflow.com/questions/29850801/subclass-pathlib-path-fails/34116756
class AppDefPath(type(FilePath()), Installable):

    @classmethod
    def __get_pydantic_core_schema__(cls, source_type: Any, handler: GetCoreSchemaHandler) -> CoreSchema:
        return core_schema.no_info_after_validator_function(
            cls, handler(FilePath)
        )

    def do_install(self, app: Application,
                   env: MutableMapping[str, Any],
                   props: Mapping[str, List[str]]) -> Iterable[FileGenerator]:
        path = app.prefix / "lib" / "inapt"
        if app.yaml is not self:
            path /= f"{app.id}.d"
            path /= self.name
        else:
            path /= f"{app.id}.yaml"
        yield CopyFileGenerator(self, path)


VolumeType = with_discriminator(union_from_entry_points("inapt.volumes"), 'volume')


class EntryPoint(BaseModel, Installable, Generic[AC]):
    features: Features = Features()
    name: Optional[str] = None
    desktop_entry: Optional[str] = None
    actions: List[AC]
    private: bool = False
    volumes: MutableMapping[IdStr, Optional[VolumeType]] = {}

    def init(self, app: Application, env: MutableMapping[str, Any], ep_id: str) -> None:
        env["ep"] = {"id": ep_id, "name": expand_single_argument(self.name, env)}
        if ep_id == "main":
            bin_name = app.id
        elif ep_id.startswith('-'):
            bin_name = f"{app.id}{ep_id}"
        else:
            bin_name = ep_id
        env["ep"]["bin_name"] = bin_name

    def compute_path(self, app, env):
        return app.prefix / "bin" / env["ep"]["bin_name"]

    def do_install(self, app: Application, env: MutableMapping[str, Any],
                   props: Mapping[str, List[str]]) -> Iterable[FileGenerator]:
        if not self.private:
            app_path = app.prefix / "lib" / "inapt" / f"{app.id}.yaml"
            from shlex import quote
            content = "#!/bin/sh\n"
            content += f"""exec {quote(env["inapt"])} {quote(str(app_path))} {quote(env["ep"]["id"])} "$@"\n"""
            yield TextFileGenerator(content, self.compute_path(app, env), 0o755)


class EntryPointNotFoundException(InaptException):
    pass


EP = TypeVar("EP", bound=EntryPoint)


class Application(BaseModel, Generic[AC, EP], ABC):
    id: str
    name: str
    target: str
    prefix: ExpandedPath = Path("/usr")
    sysconfdir: ExpandedPath = Path("/etc")
    desktop_entry: Optional[str] = None
    features: Features = Features()
    actions: Mapping[IdStr, List[AC]] = {}
    volumes: MutableMapping[IdStr, VolumeType] = {}
    entry_points: MutableMapping[IdStr, EP] = {}

    yaml: AppDefPath
    yaml_ext: Mapping[str, AppDefPath]

    def init(self):
        pass

    def prepare_environment(self) -> MutableMapping[str, Any]:
        return {"env": ChainMap({}, os.environ),
                "id": self.id,
                "name": self.name,
                "desktop_entry": self.desktop_entry,
                "prefix": self.prefix,
                "sysconfdir": self.sysconfdir,
                "inapt": "inapt"}

    def install(self,
                include: Set[PureModelPath],
                exclude: Set[PureModelPath],
                defines: DefaultDict[PureModelPath, Mapping[str, List[str]]]) -> Iterable[FileGenerator]:
        def should_process(path: PureModelPath):
            ok = len(include) == 0 or path in include
            if path in exclude:
                ok = False
            return ok

        def try_install(obj, env):
            if isinstance(obj, Installable) and should_process(path):
                props = defines[path]
                yield from obj.do_install(self, env, props)

        self.init()
        env = self.prepare_environment()

        path = PureModelPath()

        path += "features"
        for feature_name, feature in self.features.__dict__.items():
            path += feature_name
            if feature is not None and feature.enabled:
                logger.debug(f"Initializing feature {feature_name}")
                feature.init(self, None, env)
                yield from try_install(feature, env)
            path = -path
        path = -path

        path += "volumes"
        for volume_id, volume in self.volumes.items():
            path += volume_id
            logger.debug(f"Initializing volume {volume_id}")
            volume_env = ChainMap({}, env)
            volume_env[ENV_VOLUME_ID] = volume_id
            volume.init(self, None, volume_env)
            yield from try_install(volume, volume_env)
            path = -path
        path = -path

        path += "entry_points"
        for entry_point_id, entry_point in self.entry_points.items():
            path += entry_point_id
            ep_env = ChainMap({}, env)
            entry_point.init(self, ep_env, entry_point_id)
            path += "volumes"
            for volume_id, volume in entry_point.volumes.items():
                path += volume_id
                logger.debug(f"Initializing volume {entry_point_id}.{volume_id}")
                volume_env = ChainMap({}, env)
                volume_env[ENV_VOLUME_ID] = volume_id
                volume.init(self, None, volume_env)
                yield from try_install(volume, volume_env)
                path = -path
            path = -path
            yield from try_install(entry_point, ep_env)
            path = -path
        path = -path

        path += "yaml"
        yield from try_install(self.yaml, env)
        path = -path

        path += "yaml_ext"
        for yaml_ext_id, yaml_ext in self.yaml_ext.items():
            path += yaml_ext_id
            yield from try_install(yaml_ext, env)
            path = -path
        path = -path

    def run(self, entry_point: str, *args):
        self.init()
        env = self.prepare_environment()
        logger.debug(f"cmd_args are: {args}")
        env["cmd_args"] = args

        logger.debug(f"entry point is {entry_point}")
        try:
            ep = self.entry_points[entry_point]
        except KeyError:
            raise EntryPointNotFoundException(f"Entry point not found: {entry_point}")
        ep.init(self, env, entry_point)

        features = self.features.copy(update=ep.features.dict(exclude_unset=True, exclude_defaults=True))
        features = Features.parse_obj(features.dict())

        for feature_name, feature in features.__dict__.items():
            if feature is not None and feature.enabled:
                logger.debug(f"Initializing feature {feature_name}")
                feature.init(self, ep, env)

        volumes = dict(self.volumes)
        volumes.update(ep.volumes)

        for volume_id, volume in volumes.items():
            if volume is not None:
                logger.debug(f"Initializing volume {volume_id}")
                volume.init(self, ep, env)

        with ExitStack() as stack:
            for volume_id, volume in volumes.items():
                if volume is not None:
                    volume_env = ChainMap({}, env)
                    volume_env[ENV_VOLUME_ID] = volume_id
                    logger.debug(f"Setting up volume {volume_id}")
                    stack.enter_context(volume.setup(self, ep, volume_env))

            self.volumes_ready(ep, env)

            for feature_name, feature in features.__dict__.items():
                if feature is not None and feature.enabled:
                    logger.debug(f"Setting up feature {feature_name}")
                    feature.setup(self, ep, env)

            for action in self.transform_actions(ep.actions, ep, env):
                logger.debug(f"Running action {action}")
                action_env = ChainMap({}, env)
                action.run(self, ep, action_env)

    def volumes_ready(self, ep: EP, env: MutableMapping[str, Any]):
        pass

    def transform_actions(self, actions: List[AC], ep: EP, env: MutableMapping[str, Any]) -> Iterable[Action]:
        return actions


ApplicationType = with_discriminator(union_from_entry_points("inapt.targets"), 'target')


class Extension(BaseModel):
    id: IdStr
    priority: int
    application: Mapping[str, Any]

class RootExtension(BaseModel):
    extension: Extension


class Root(BaseModel):
    application: ApplicationType
