/*
Copyright (c) 2020-2023 Florian Léger

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/
*/

/* Based on Enlightenment's libehack code */

#include <stdlib.h>
#include <dlfcn.h>
#include <X11/Xlib.h>
#include <X11/X.h>
#include <X11/Xutil.h>

/* prototypes */
static void __inapt_set_properties(Display *display, Window window);

/* dlopened xlib so we can find the symbols in the real xlib to call them */
static void *lib_xlib = NULL;

/* the function that actually sets the properties on toplevel window */
static void __inapt_set_properties(Display *display, Window window) {
    char *env = NULL;

    if((env = getenv("INAPT_WMCLASS"))) {
        XClassHint *classhint;
        classhint = XAllocClassHint();
        
        if(classhint) {
            classhint->res_name = env;
            classhint->res_class = env;
            XSetClassHint(display, window, classhint);
            XFree(classhint);
        }
    }
}

/* XCreateWindow intercept hack */
Window XCreateWindow(Display *display,
                     Window parent,
                     int x, int y,
                     unsigned int width, unsigned int height,
                     unsigned int border_width,
                     int depth,
                     unsigned int class,
                     Visual *visual,
                     unsigned long valuemask,
                     XSetWindowAttributes *attributes) {
    static Window (*func) (Display *display,
                           Window parent,
                           int x, int y,
                           unsigned int width, unsigned int height,
                           unsigned int border_width,
                           int depth,
                           unsigned int class,
                           Visual *visual,
                           unsigned long valuemask,
                           XSetWindowAttributes *attributes) = NULL;
    int i;

    /* find the real Xlib and the real X function */
    if (!lib_xlib) lib_xlib = dlopen("libX11.so", RTLD_GLOBAL | RTLD_LAZY);
    if (!func) func = dlsym(lib_xlib, "XCreateWindow");

    /* multihead screen handling loop */
    for (i = 0; i < ScreenCount(display); i++) {
        /* if the window is created as a toplevel window */
        if (parent == RootWindow(display, i)) {
            Window window;
            
            /* create it */
            window = (*func) (display, parent, x, y, width, height, 
                    border_width, depth, class, visual, valuemask, 
                    attributes);
            /* set properties */
            __inapt_set_properties(display, window);
            /* return it */
            return window;
        }
    }
    /* normal child window - create as usual */
    return (*func) (display, parent, x, y, width, height, border_width, depth,
            class, visual, valuemask, attributes);
}

/* XCreateSimpleWindow intercept hack */
Window XCreateSimpleWindow(Display *display,
                           Window parent,
                           int x, int y,
                           unsigned int width, unsigned int height,
                           unsigned int border_width,
                           unsigned long border,
                           unsigned long background) {
    static Window (*func) (Display *display,
                           Window parent,
                           int x, int y,
                           unsigned int width, unsigned int height,
                           unsigned int border_width,
                           unsigned long border,
                           unsigned long background) = NULL;
    int i;
    
    /* find the real Xlib and the real X function */
    if (!lib_xlib) lib_xlib = dlopen("libX11.so", RTLD_GLOBAL | RTLD_LAZY);
    if (!func) func = dlsym(lib_xlib, "XCreateSimpleWindow");
    
    /* multihead screen handling loop */
    for (i = 0; i < ScreenCount(display); i++) {
        /* if the window is created as a toplevel window */
        if (parent == RootWindow(display, i)) {
            Window window;
            
            /* create it */
            window = (*func) (display, parent, x, y, width, height, 
                    border_width, border, background);
            /* set properties */
            __inapt_set_properties(display, window);
            /* return it */
            return window;
        }
    }
    /* normal child window - create as usual */
    return (*func) (display, parent, x, y, width, height, 
            border_width, border, background);
}

/* XReparentWindow intercept hack */
int XReparentWindow(Display *display,
                    Window window,
                    Window parent,
                    int x, int y) {
    static int (*func) (Display *display,
                        Window window,
                        Window parent,
                        int x, int y) = NULL;
    int i;
    
    /* find the real Xlib and the real X function */
    if (!lib_xlib) lib_xlib = dlopen("libX11.so", RTLD_GLOBAL | RTLD_LAZY);
    if (!func) func = dlsym(lib_xlib, "XReparentWindow");
    
    /* multihead screen handling loop */
    for (i = 0; i < ScreenCount(display); i++) {
        /* if the window is created as a toplevel window */
        if (parent == RootWindow(display, i)) {
            /* set properties */
            __inapt_set_properties(display, window);
            /* reparent it */
            return (*func) (display, window, parent, x, y);
        }
    }
    /* normal child window reparenting - reparent as usual */
    return (*func) (display, window, parent, x, y);
}
