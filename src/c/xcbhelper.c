/*
Copyright (c) 2020-2023 Florian Léger

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/
*/

#include <stdlib.h>
#include <dlfcn.h>
#include <string.h>

#include <xcb/xproto.h>

/* dlopened libxcb so we can find the symbols in the real libxcb to call them */
static void *lib_libxcb = NULL;

/* xcb_change_property intercept hack (for qt apps) */
xcb_void_cookie_t
xcb_change_property (xcb_connection_t *c,
                     uint8_t           mode,
                     xcb_window_t      window,
                     xcb_atom_t        property,
                     xcb_atom_t        type,
                     uint8_t           format,
                     uint32_t          data_len,
                     const void       *data) {

    static xcb_void_cookie_t (*func) (
                     xcb_connection_t  *c,
                     uint8_t           mode,
                     xcb_window_t      window,
                     xcb_atom_t        property,
                     xcb_atom_t        type,
                     uint8_t           format,
                     uint32_t          data_len,
                     const void       *data
    ) = NULL;

    xcb_get_atom_name_reply_t *reply = NULL;
    char *name = NULL;
    int name_length;
    char* new_data;
    uint32_t new_length = 0;
    char *env = NULL;
    xcb_void_cookie_t result;

    /* find the real libxcb and the real xcb function */
    if (!lib_libxcb) lib_libxcb = dlopen("libxcb.so", RTLD_GLOBAL | RTLD_LAZY);
    if (!func) func = dlsym(lib_libxcb, "xcb_change_property");

    if (mode == XCB_PROP_MODE_REPLACE) {
        reply = xcb_get_atom_name_reply(c, xcb_get_atom_name(c, property), NULL);
        name = xcb_get_atom_name_name(reply);
        name_length = xcb_get_atom_name_name_length(reply);
        if (strncmp("WM_CLASS", name, name_length) == 0) {
            if((env = getenv("INAPT_WMCLASS"))) {
                new_length = (strlen(env) + 1) * 2;
                new_data = (char*) calloc(new_length, sizeof(char));
                strcpy(new_data, env);
                strcpy(&new_data[strlen(env) + 1], env);
                result = (*func) (c, mode, window, property, type, format, new_length, new_data);
                free(new_data);
                return result;
            }
        }
    }

    return (*func) (c, mode, window, property, type, format, data_len, data);
}

