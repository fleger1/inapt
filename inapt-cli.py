#!/usr/bin/env python3

# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'src', 'python'))

from inapt.cli import main

if __name__ == "__main__":
    main()
