#!/usr/bin/env python

# Copyright (c) 2020-2023 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import platform
from pathlib import Path

from setuptools import setup, find_packages, Command

import versioneer


class build_helpers(Command):
    description = "build C/C++ helpers"

    user_options = [
        ('build-temp=', 't',
         "directory to put temporary build by-products"),
        ('build-lib=', 'b',
         "directory for compiled extension modules"),
    ]

    def initialize_options(self):
        self.build_temp = None
        self.build_lib = None

    def finalize_options(self):
        self.set_undefined_options('build',
                                   ('build_lib', 'build_lib'),
                                   ('build_temp', 'build_temp'))

    def build_project(self, toolchain=None):
        lib_path = (Path(self.build_lib) / "inapt" / "helpers").absolute()

        self.announce(f"Configuring CMake project with {toolchain if toolchain else 'default'} toolchain", level=3)
        build_path = str(Path(self.build_temp) / f"build{toolchain if toolchain else ''}")
        cmd = ["cmake", "-S", "src/c/", "-B", build_path, f"-DINSTALL_LIBDIR={lib_path}"]
        if toolchain:
            toolchain_path = Path(f"src/c/cmake/toolchain-{toolchain}.cmake").absolute()
            cmd.append(f"-DCMAKE_TOOLCHAIN_FILE={toolchain_path}")
        self.spawn(cmd)

        self.announce("Building helper libraries", level=3)
        self.spawn(["cmake", "--build", build_path])

        self.announce("Installing helper libraries", level=3)
        self.spawn(["cmake", "--install", build_path])

    def run(self):
        # Build for the native architecture
        self.build_project()
        if platform.system() == "Linux" and platform.machine() == "x86_64":
            # If x86_64 also build for i686 to support 32-bit binaries
            self.build_project("linux-i686-on-x86_64")


cmd_class = versioneer.get_cmdclass()


# Make sure we build the so helpers
class inapt_build_py(cmd_class['build_py']):

    def run(self) -> None:
        super().run()
        self.run_command('build_helpers')


cmd_class['build_py'] = inapt_build_py
cmd_class['build_helpers'] = build_helpers

setup(
    name="inapt",
    version=versioneer.get_version(),
    packages=find_packages("src/python"),
    package_dir={'': 'src/python'},
    install_requires=['pydantic>=2.3.0', 'PyYAML', 'pyxdg', 'pyparsing'],
    extras_require={
        'relayer': ["relayer"],
        'legendary': ["legendary-gl"]
    },
    license="MPL 2.0",
    author="Florian Léger",
    author_email="florian.leger6@gmail.com",
    description="Non-native application launcher",
    keywords="launcher wrapper script flashplayer renpy scummvm dosbox nw.js wine fuse ags",
    cmdclass=cmd_class,
    entry_points={
        'inapt.volumes': [
            "system = inapt.volumes:SystemVolume",
            "xdg = inapt.volumes:XDGVolume",
            "relayer = inapt.volumes:RelayerVolume",
            "legendary = inapt.ext.legendary:LegendaryVolume",
        ],
        'inapt.features': [
            "wm_class = inapt.features:WMClassFeature",
            "sdl = inapt.features:SDLFeature",
            "ldpreloadhelper = inapt.features:LDPreloadHelperFeature",
            "gtk = inapt.features:GTKFeature",
            "discrete_gpu = inapt.features:DiscreteGPUFeature",
            "legendary = inapt.ext.legendary:LegendaryFeature",
        ],
        'inapt.targets': [
            "generic = inapt.targets:GenericApplication",
            "dosbox = inapt.targets.dosbox:DOSBoxApplication",
            "wine = inapt.targets.wine:WineApplication",
            "scummvm = inapt.targets.scummvm:ScummVMApplication",
            "fuse = inapt.targets.fuse:FuseApplication",
            "ags = inapt.targets.ags:AGSApplication",
            "vbam = inapt.targets.vbam:VBAMApplication",
            "renpy = inapt.targets.renpy:RenPyApplication",
            "mame = inapt.targets.mame:MameApplication",
            "gargoyle = inapt.targets.gargoyle:GargoyleApplication",
            "nwjs = inapt.targets.nwjs:NWjsApplication",
            "flashplayer = inapt.targets.flashplayer:FlashPlayerApplication",
            "mgba = inapt.targets.mgba:MGBAApplication",
        ],
        'inapt.actions.common': [
            "exec = inapt.actions:ExecAction",
            "ref = inapt.actions:RefAction",
            "shell = inapt.actions:ShellAction",
            "temp_script = inapt.actions:TempScriptAction",
        ],
        'inapt.scripting.common': [
            "script_ref = inapt.scripting:ScriptRefTemplate",
        ],
        "inapt.targets.dosbox.templates": [
            "if_not_exist = inapt.targets.dosbox:IfNotExistTemplate",
            "if_exist = inapt.targets.dosbox:IfExistTemplate",
            "die_if_not_exist = inapt.targets.dosbox:DieIfNotExistTemplate",
            "env_copy = inapt.targets.dosbox:EnvCopyTemplate",
        ],
        "inapt.targets.wine.templates": [
            "registry = inapt.targets.wine:RegistryTemplate",
            "if_not_exist = inapt.targets.wine:IfNotExistTemplate",
            "if_exist = inapt.targets.wine:IfExistTemplate",
            "die_if_not_exist = inapt.targets.wine:DieIfNotExistTemplate",
        ],
        'console_scripts': [
            'inapt = inapt.cli:main',
            'inapt-install = inapt.installcli:main',
        ],
    },
    # TODO: add project URL
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3.9",
        "Topic :: Software Development :: Code Generators",
        "Topic :: System :: Software Distribution",
    ],
    # setup_requires=['pytest-runner'],
    # tests_require=['pytest-mock'],
)
