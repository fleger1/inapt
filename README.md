# inapt

An application launcher.


# Installation

## Prerequisites

inapt needs the following software packages to run:
* [Python 3.9 or later](https://www.python.org/)
* [setuptools](https://pypi.org/project/setuptools/)
* [PyXDG](https://pypi.org/project/pyxdg/)
* [pydantic 1.9.0 or later](https://github.com/samuelcolvin/pydantic/)
* [PyYAML](https://pyyaml.org/)

Optional runtime dependencies:
* [Relayer](https://gitlab.com/fleger1/relayer): for using relayer volumes
* [libx11](https://xorg.freedesktop.org/): for using the x11 ldpreload helper feature. On x86_64 systems you'll probably want
  to install the 32-bit version too.
* [libxcb](https://xcb.freedesktop.org/): for using the xcb ldpreload helper feature. On x86_64 systems you'll probably want
  to install the 32-bit version too.
* [legendary](https://github.com/derrod/legendary): for interacting with games from the Epic Games platform

Additionally, the following are required in order to build inapt:
* [cmake](https://cmake.org/) 3.16 or higher
* [Extra CMake Modules](https://invent.kde.org/frameworks/extra-cmake-modules)
* [libx11](https://xorg.freedesktop.org/)
* [libxcb](https://xcb.freedesktop.org/)
* [gcc](https://gcc.gnu.org/)


## Manual installation

Clone the Git repository or download and extract a release tarball and
run the following commands:

```shell
./setup.py build
./setup.py install # May require root privileges
```

## Installation using pip

pip installation is not yet supported.

# Usage

To launch the entrypoint ```main``` of the application described in ```/path/to/my_application.yaml```:
```shell
inapt /path/to/my_application.yaml main
```

# Overview

inapt parses applications described in YAML files in order to launch entry points declared there.

Let's say we want to create a launcher for Commander Keen 1, a shareware DOS game.
You can grab it [here](http://cd.textfiles.com/smsharew/GAMES1/1KEENV3.ZIP).
Since it's a DOS application we'll use DOSBox.

We want to put the game data in /usr and have per-user savegames.

Here's the inapt application file:


```yaml
application:
  id: keen1
  name: "Commander Keen Episode 1: Marooned on Mars"
  desktop_entry: fleger.games.id.keen1
  target: dosbox
  volumes:
    data: {volume: relayer}
  mount_points:
    "C:":
      volume: data
  init_script:
    - "C:"
    - cd \
  entry_points:
    main:
      actions:
        - ["KEEN1.EXE", "{cmd_args}"]
```
First we declare an application with the following attributes:
* **id:** an application identifier. This is used for figuring out were the application data is stored.
* **name:** the full name of the application (optional). The dosbox target uses this value to set the window title.
* **desktop_entry:** the name of the [XDG Desktop Entry](https://specifications.freedesktop.org/desktop-entry-spec/latest/) that may be used to launch the application (optional).
  Some targets (including ```dosbox```) will use this value to make sure that your desktop environment is aware that the application running is linked
  to a XDG Desktop Entry, which allows cool features like window grouping and application pinning to
  work as intended. 
* **target:** the type of application we are targeting. Some attributes and behaviors are target-specific.
  inapt support the following targets out-of-the-box:
  * **generic:** useful for running native applications
  * **ags:** for launching games made with [Adventure Game Studio](https://www.adventuregamestudio.co.uk/)
  * **dosbox:** for launching DOS applications using [DOSBox](https://www.dosbox.com/)
  * **flashplayer:** for launching Flash games using the standalone [Flash Player](https://www.adobe.com/support/flashplayer/debug_downloads.html)
  * **fuse:** for launching ZX Spectrum games using the [Free Unix Spectrum Emulator](http://fuse-emulator.sourceforge.net/fuse.php)
  * **gargoyle:** for playing interactive fictions using [Gargoyle](https://github.com/garglk/garglk)
  * **mame:** for running software using [Mame](https://www.mamedev.org/)
  * **nwjs:** for running web applications using [NW.js](https://nwjs.io/)
  * **renpy:** for playing [Ren'Py](https://www.renpy.org/) visual novels
  * **residualvm:** for playing 3D games supported by [ResidualVM](https://www.residualvm.org/)
  * **scummvm:** for playing 2D games supported by [ScummVM](https://www.scummvm.org/)
  * **vbam:** for playing Game Boy Advance games with [Visual Boy Advance-M](https://vba-m.com/)
  * **wine:** for running Windows applications using [Wine](https://www.winehq.org/)
  
* **volumes:** volumes are places where the application can find and/or store data. Multiple volumes can be defined per
  application. In our example, only one volume with id ```data``` and of type ```relayer``` is specified. The following volume types
  are supported out of the box:
  * **system:** specify a path on the file system (most of the time this will be a directory). This is useful for specifying 
    where the static, readonly application data is stored. If the path is not explicitly specified, it
    will default to ```{prefix}/share/{id}/{volume_dir}```, where ```{prefix}``` is the application prefix (```/usr``` by default),
    ```{id}``` the application id and ```{volume_dir}``` is equal to the volume id unless the id is "default", in which case ```{volume_dir}``` is empty.
  * **xdg:** a user-specific path on the file system. This is useful for describing where to store user data (application
    preferences, saves...), for applications that have a clean separation between static data and user data. The paths
    generated by this volume type adhere to the [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html).
    By default, ```$XDG_DATA_HOME/{id}/{volume_dir}``` is used. Only directories are supported by this volume type.
  * **relayer:** specify a [relayer-managed](https://gitlab.com/fleger1/relayer) volume. There are two use cases when you might want
    to use a relayer volume:
      * The application does not have a clean separation between static application data and user-specific data.
        This is the case with this example: Commander Keen 1 will attempt to write save games and high scores in the same
        directory as the game data and executable. Relayer will make the application "believe" that it can write files
        in the application data directory.
      * You want to implement a simple mod management system for a game.
    
    By default, inapt uses ```{id}_{volume_dir}``` as relayer application name, or simply ```{id}``` if the volume id is "default". 
* **mount_points:** allows to map volumes to DOS drives. This is specific to the ```dosbox``` and ```wine``` targets.
  In this example, we map the ```data``` volume declared earlier to the ```C:``` drive. By default, the volume will be
  seen as a hard drive. For the ```dosbox``` target, it is not allowed to map a volume to ```Z:``` since it used internally by DOSBox.
  
* **init_script:** list of DOS commands or script templates to run right after mounting the data volumes, but before
  executing other commands defined at entry point level. Optional, specific to the ```dosbox``` target.
  Strings and list of strings are subject to brace expansion. This allow performing common initialization steps each
  time a dosbox ```batch``` action is processed.
  
* **entry_points:** an application is launched through entry points. There can be multiple entry points per application,
  but each must have a different id. In this example, only one entry point, whose id is ```main```, is defined.
  An entry point must define a list of ```actions```. Each action has a type. In addition to common actions, each ```target```
  can define additional specific types of action that can be used, and ways to use them conveniently. For the ```dosbox``` target,
  consecutive strings, list of strings and script templates are automatically grouped and translated into ```batch``` actions, which
  are responsible for launching a DOSBox instance, mounting volumes, executing ```init_script``` and script content defined or referenced at
  entry point level.
  
inapt can handle much more complicated scenarios, such as:
* running executable conditionaly (think running a setup executable the first time you launch a game),
* CPU pinning,
* action referencing for improved reusability,
* script referencing for improved reusability,
* winetricks integration for the ```wine``` target,
* defining additional entry points or overloading behaviors  through application extensions,
* more!

# License

inapt is licensed under the terms of the Mozilla Public License Version 2.0.
