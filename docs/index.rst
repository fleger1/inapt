.. inapt documentation master file, created by
   sphinx-quickstart on Sun Aug 23 08:48:15 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to inapt's documentation!
=================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   install
   support
   ags_cookbook
   dosbox_cookbook
   flashplayer_cookbook
   fuse_cookbook
   gargoyle_cookbook
   mame_cookbook
   nwjs_cookbook
   renpy_cookbook
   scummvm_cookbook
   vbam_cookbook
   wine_cookbook

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
