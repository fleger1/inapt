MAME Target Cookbook
====================

Simple MAME game
----------------

Application definition for `Ultima Trilogy: I ♦ II ♦ III`_ for the FM-Towns platform.
The ``save`` volumes is used to store the savegames into a floppy image created by the external
``make-fmtowns-empty-fat-floppy`` tool. The ``roms`` volume points to the location of the FM-Towns ROMs, while
the ``cdrom`` volume points to the game CD image.

.. code-block:: yaml
    :caption: ultima123.yaml

    application:
      target: mame
      id: ultima123
      name: "Ultima Trilogy: I ♦ II ♦ III"
      desktop_entry: "{id}"
      volumes:
        save:
          volume: xdg
        roms:
          volume: system
          path: "{prefix}/share/mame/roms"
        cdrom:
          volume: system
          path: "{prefix}/share/{id}/{id}.chd"
      rompath: "{volumes[roms]}"
      games:
        ultima123:
          machine: fmtowns
          media:
            flop1: "{volumes[save]}/disk0.hdm"
            cdrm: "{volumes[cdrom]}"
      entry_points:
        main:
          actions:
            - action: exec
              command: ["make-fmtowns-empty-fat-floppy", "{volumes[save]}/disk0.hdm"]
            - ultima123


.. _`Ultima Trilogy: I ♦ II ♦ III`: https://www.mobygames.com/game/fmtowns/ultima-trilogy-i-ii-iii