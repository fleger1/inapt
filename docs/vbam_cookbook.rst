VBA-M Target Cookbook
=====================

Simple Game Boy Advance game
----------------------------

Application definition for `Pokémon FireRed`_.

.. code-block:: yaml
    :caption: pokemon-firered.yaml

    application:
      target: vbam
      id: pokemon-firered
      name: Pokémon FireRed
      desktop_entry: "{id}"
      volumes:
        data: {volume: system}
      entry_points:
        main:
          actions:
            - "{volumes[data]}/{id}.gba.gz"


.. _`Pokémon FireRed`: https://www.mobygames.com/game/gameboy-advance/pokmon-firered-version