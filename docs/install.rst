Installation
============

Prerequisites
-------------

inapt needs the following software packages to run:

* Python_ 3.8 or later
* setuptools_
* PyXDG_
* pydantic_
* PyYAML_

Optional runtime dependencies:

* Relayer_: for using relayer volumes
* libx11_: for using the x11helper feature. On x86_64 systems you'll probably want
  to install the 32-bit version too.

Additionally, the following are required in order to build inapt:

* cmake_
* libx11_
* gcc_

Manual installation
-------------------

Clone the Git repository or download and extract a release tarball and
run the following commands:

.. code-block:: bash

    ./setup.py build
    ./setup.py install # May require root privileges

Installation using pip
----------------------

pip installation is not yet supported.

.. _Python: https://www.python.org/
.. _setuptools: https://pypi.org/project/setuptools/
.. _PyXDG: https://pypi.org/project/pyxdg/
.. _pydantic: https://github.com/samuelcolvin/pydantic/
.. _PyYAML: https://pyyaml.org/
.. _Relayer: https://gitlab.com/fleger1/relayer
.. _libx11: https://xorg.freedesktop.org/
.. _cmake: https://cmake.org/
.. _gcc: https://gcc.gnu.org/