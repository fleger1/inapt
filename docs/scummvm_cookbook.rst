ScummVM Target Cookbook
====================================

Simple ScummVM game
-------------------

Application definition for `Maniac Mansion`_ (Amiga version).

.. code-block:: yaml
    :caption: maniac.yaml

    application:
      target: scummvm
      id: maniac
      name: "Maniac Mansion"
      desktop_entry: "{id}"
      volumes:
        data: {volume: system}
      games:
        maniac:
          target: maniac-v2-amiga
          path: "{volumes[data]}"
      entry_points:
        main:
          actions:
            - maniac

.. _`Maniac Mansion`: https://www.mobygames.com/game/amiga/maniac-mansion
