Gargoyle Target Cookbook
========================

Simple interactive fiction
--------------------------

Application definition for `Zork I: The Great Underground Empire`_.

.. code-block:: yaml
    :caption: zork1.yaml

    application:
      target: gargoyle
      id: zork1
      name: "Zork I: The Great Underground Empire"
      desktop_entry: "{id}"
      volumes:
        data: {volume: system}
      entry_points:
        main:
          actions:
            - "{volumes[data]}/{id}.z5"


.. _`Zork I: The Great Underground Empire`: https://www.mobygames.com/game/zork-the-great-underground-empire