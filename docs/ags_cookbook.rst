Adventure Game Studio Target Cookbook
=====================================

Simple AGS application
----------------------

Application definition for `Quest for Glory II: Trial by Fire (AGD Interactive Remake)`_, including call to external
``qfg-import-heroes`` script to handle character transfer from QfG1.

.. code-block:: yaml
    :caption: qfg2remake.yaml

    application:
      target: ags
      id: qfg2remake
      name: "Quest for Glory II: Trial by Fire"
      desktop_entry: "{id}"
      volumes:
        data:
          volume: relayer
        qfg1:
          volume: xdg
          application: scummvm
          subdirectory: saves
      entry_points:
        main:
          actions:
            - action: exec
              command: [qfg-import-heroes, "{volumes[qfg1]}", "{volumes[data]}", "qfg1-*.sav"]
            - "{volumes[data]}"

.. _`Quest for Glory II: Trial by Fire (AGD Interactive Remake)`: http://www.agdinteractive.com/games/qfg2/homepage/homepage.html