Free Unix Spectrum Emulator Studio Target Cookbook
==================================================

Simple FUSE application
-----------------------

Application definition for `Nohzdyve`_.

.. code-block:: yaml
    :caption: nohzdyve.yaml

    application:
      target: fuse
      id: nohzdyve
      name: Nohzdyve
      desktop_entry: "{id}"
      volumes:
        data: {volume: system}
      entry_points:
        main:
          actions:
            - "{volumes[data]}/{id}.tap"


.. _`Nohzdyve`: https://tuckersoft.net/ealing20541/nohzdyve/