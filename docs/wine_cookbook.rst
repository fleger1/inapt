Wine Target Cookbook
====================

Simple win32 application
------------------------

Application definition for `Distraint: Deluxe Edition`_.

.. code-block:: yaml
    :caption: distraintdeluxe.yaml

    application:
      target: wine
      id: distraintdeluxe
      name: "Distraint: Deluxe Edition"
      desktop_entry: "{id}"
      volumes:
        data: {volume: system}
      wine_arch: win32
      mount_points:
        "D:":
          volume: data
      entry_points:
        main:
          actions:
            - [D:\distraintdeluxe.exe, "{cmd_args}"]

Set CPU affinity
----------------

Application definition for `Afterlife`_. The game has sound issues on modern multi-core systems, so we work around
this limitation by pinning the game process to the first CPU. Note that the ``cpu_affinity`` option
is available for any action spawning a child process.

.. code-block:: yaml
    :caption: afterlife.yaml
    :emphasize-lines: 18

    application:
      target: wine
      id: afterlife
      name: "Afterlife"
      desktop_entry: "{id}"
      volumes:
        data: {volume: relayer}
      wine_arch: win32
      mount_points:
        "D:":
          volume: data
      entry_points:
        main:
          actions:
            - action: cmd
              # On SMP systems sound drops out during gameplay.
              # Work around this issue by pinning the process to the first CPU.
              cpu_affinity: [0]
              script:
                - [D:\ALIFE.EXE, "{cmd_args}"]

Epic Games
----------

Integration with the Epic Game Launcher is done using the `legendary`_ feature.
Legendary must be installed in order to use this feature.
This feature takes two mandatory parameters:

* ``app_name`` is the Epic application name. It can be found by running ``legendary list-games``.
* ``app_path`` is the path where the game assets are located.

The feature will populate two variables:

* ``legendary[cwd]`` contains the working directory of the game.
* ``legendary[params]`` contains the command to be executed by wine in order to launch the game.

Please note that the user will still need to be authenticated to launch the game (see ``legendary auth``).

Example for `Crying Suns`_:

.. code-block:: yaml
    :caption: cryingsuns.yaml
    :emphasize-lines: 9-11,17,21

    application:
      target: wine
      id: cryingsuns
      name: "Crying Suns"
      desktop_entry: "{id}"
      dxvk:
        enabled: yes
      features:
        legendary:
          app_name: 18fafa2d70d64831ab500a9d65ba9ab8
          app_path: "{volumes[default]}"
      volumes:
        default: {volume: system}
      wine_arch: win64
      init_script:
        - "Z:"
        - [cd, "{legendary[cwd]}"]
      entry_points:
        main:
          actions:
            - ["{legendary[params]}", "{cmd_args}"]

DXVK
----
Provided that it is installed, `DXVK`_ can be enabled by adding the following bit under the application section:

.. code-block:: yaml

      dxvk:
        enabled: yes

This will make inapt the dxvk dlls in the wine prefix before launching the application.
Use ``dxvk_path`` to indicate where dxvk is installed.

.. _`Afterlife`: https://www.mobygames.com/game/windows/afterlife/
.. _`Distraint: Deluxe Edition`: http://www.jessemakkonen.com/
.. _`Legendary`: https://github.com/derrod/legendary
.. _`Crying Suns`: https://www.epicgames.com/store/en-US/product/crying-suns/home
.. _`DXVK`: https://github.com/doitsujin/dxvk/

Discrete GPU
------------
You can use the ``discrete_gpu`` feature yo make the application use a discrete GPU when available instead of the default
one:

.. code-block:: yaml

    application:
      ...
      features:
        discrete_gpu: {}

The following methods are supported by default:

- If `Bumblebee`_ is installed, the application will be launched using either ``primerun``  or ``optirun``.

- Failing that, relevant environment variables will be set to make use of the PRIME Offloading feature from the open
  source drivers and from NVidia's proprietary driver.

.. _`Bumblebee`: https://www.bumblebee-project.org/
