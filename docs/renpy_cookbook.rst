Ren'Py Target Cookbook
======================

Simple Ren'Py game
------------------

Application definition for `Sunrider: Mask of Arcadius`_.

.. code-block:: yaml
    :caption: sunrider-moa.yaml

    application:
      target: renpy
      id: sunrider-moa
      name: "Sunrider: Mask of Arcadius"
      desktop_entry: "{id}"
      volumes:
        data: {volume: system}
        save: {volume: xdg}
      games:
        sunrider-moa:
          basedir: "{volumes[data]}"
          savedir: "{volumes[save]}"
      entry_points:
        main:
          actions:
            - sunrider-moa


.. _`Sunrider: Mask of Arcadius`: https://www.mobygames.com/game/sunrider-mask-of-arcadius